# @universis/ngx-im

An implementation of instant messaging for [Universis project](https://gitlab.com/universis).

## Installation

Install @universis/ngx-im in an angular cli by executing:

    npm i @universis/ngx-im

Important note: Don't forget to install peer dependencies also.
## Import `NgxModule`:
We can use a new custom module with an optional configuration as shown below:

    import { NgModule } from '@angular/core';
    import { NgxImModule } from '@universis/ngx-im';

    @NgModule({
    imports: [
        NgxImModule
        .forRoot({
            MIN_HEIGHT_IM: 'inherit', // min height of instant messaging component
            MAX_HEIGHT_IM: '70vh', // max height of instant messaging component
            MAX_WIDTH_MESSAGES: '40%', // max width of messages
            MAX_HEIGHT_REPLIES: '50vh' // max height of replies modal
        })
    ]
    })
    export class ImModule { }
The instant messaging module handles by itself all the necessary routings and paths using it's own routing modules.
## Usage
On the target module you can attach it at the routes section as showing below:

    const routes: Routes = [
        {
        path: '',
        component: HomeComponent,
        data: {
            title: 'Home'
        },
        children: [
                {
                    path: '',
                    loadChildren: () => import('../im/im.module').then(m => m.ImModule)
                }
            ]
        }
    ];

Passing the module inside as a route, allows you to handle it as a page and as a component in the DOM. A simple example is showing below:

    <div>Section 1</div>
    <div class="d-flex">
        <div>Section 2</div>
        <router-outlet></router-outlet>
    </div>
    <div>Section 3</div>

If you want to render it as a single page you can simple load the module with your desired path prefix as showing below:

    const routes: Routes = [
        {
            path: '',
            component: HomeComponent,
            data: {
                title: 'Home'
            },
        },
        {
            path: 'my-path',
            loadChildren: () => import('../im/im.module').then(m => m.ImModule)
        }
    ];
