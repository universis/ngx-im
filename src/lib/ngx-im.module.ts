import { LOCALE_ID, Inject, ModuleWithProviders, NgModule } from '@angular/core';
import { LayoutComponent } from './layout/layout.component';
import { NgxImRoutingModule } from './ngx-im-routing.module';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { ContentActionsComponent } from './components/content/content-actions/content-actions.component';
import { ContentComponent } from './components/content/content.component';
import { CommonModule } from '@angular/common';
import { EmptyContentComponent } from './components/empty-content/empty-content.component';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { AppHeaderModule, AppSidebarModule } from '@coreui/angular';
import { HeaderComponent } from './components/header/header.component';
import { DATA_CONTEXT_CONFIG, AngularDataContext } from '@themost/angular';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { MembersChannelComponent } from './components/modals/members-channel/members-channel.component';
import { HideChannelComponent } from './components/modals/hide-channel/hide-channel.component';
import { BsModalService, ModalModule } from 'ngx-bootstrap/modal';
import { AddEditMemberChannelComponent } from './components/modals/add-edit-member-channel/add-edit-member-channel.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaginationPipePipe } from './pipes/pagination-pipe.pipe';
import { NewMembersTableComponent } from './components/modals/components/new-members-table/new-members-table.component';
import { CurrentMembersTableComponent } from './components/modals/components/current-members-table/current-members-table.component';
import { ContentMessagesComponent } from './components/content/content-messages/content-messages.component';
import { QuillModule } from 'ngx-quill';
import { NgxImComponent } from './ngx-im.component';
import { AddChannelComponent } from './components/modals/add-channel/add-channel.component';
import { EditChannelComponent } from './components/modals/edit-channel/edit-channel.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ContentThreadComponent } from './components/modals/content-thread/content-thread.component';
import { AlertModule } from 'ngx-bootstrap/alert';
import { ErrorComponent } from './components/error/error.component';
import { NoSanitizePipe } from './pipes/noSanitize-pipe.pipe';
import { IConfig } from './interfaces/IConfig';
import { APP_CONFIG } from './ngx-im.config';
import { CastPipe } from './pipes/cast-pipe.pipe';
import { ModalService, ServerEventModule } from '@universis/common';
import { SidebarRightComponent } from './components/sidebar-right/sidebar-right.component';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { IM_LOCALES } from './i18n';
import { ViewMemberComponent } from './components/modals/view-member/view-member.component';
import { ImPreferencesService } from './services/im-preferences.service';
import { ViewAttachmentComponent } from './components/modals/view-attachment/view-attachment.component';
import { InstantNotificationComponent } from './components/modals/instant-notification/instant-notification.component';
import { AddChannelRouterComponent } from './components/modals/add-channel-router/add-channel-router.component';
import { RouterModalModule } from '@universis/common/routing';
import { AdvancedFormsModule } from '@universis/forms';
import { NgxImSharedModule } from './ngx-im.shared.module';
import { AddChannelService } from './services/add-channel.service';
import { DataService } from './services/data.service';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

@NgModule({
  declarations: [
    NgxImComponent,
    LayoutComponent,
    SidebarComponent,
    ContentComponent,
    ContentActionsComponent,
    EmptyContentComponent,
    HeaderComponent,
    MembersChannelComponent,
    HideChannelComponent,
    AddEditMemberChannelComponent,
    PaginationPipePipe,
    NoSanitizePipe,
    CastPipe,
    NewMembersTableComponent,
    CurrentMembersTableComponent,
    ContentMessagesComponent,
    AddChannelComponent,
    EditChannelComponent,
    ContentThreadComponent,
    ErrorComponent,
    SidebarRightComponent,
    ViewMemberComponent,
    ViewAttachmentComponent,
    InstantNotificationComponent,
    AddChannelRouterComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    TranslateModule.forRoot(),
    NgxImSharedModule,
    AppHeaderModule,
    AppSidebarModule,
    PerfectScrollbarModule,
    NgxImRoutingModule,
    ReactiveFormsModule,
    AdvancedFormsModule,
    RouterModalModule,
    AlertModule.forRoot(),
    BsDropdownModule.forRoot(),
    PaginationModule.forRoot(),
    ModalModule.forRoot(),
    QuillModule.forRoot(),
    TooltipModule.forRoot(),
  ],
  providers: [
    {
      provide: DATA_CONTEXT_CONFIG, useValue: {
        base: 'http://localhost:5001/api/',
        options: {
          useMediaTypeExtensions: false,
          useResponseConversion: true,
        },
      },
    },
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    },
    AngularDataContext,
    DataService,
    ModalService,
    AddChannelService,
  ],
  exports: []
})

export class NgxImModule {
  constructor(@Inject(LOCALE_ID) public locale: string,
              private _translateService: TranslateService) {
    this.ngOnInit().catch(err => {
      console.error('An error occurred while loading NgxImModule module');
      console.error(err);
    });
    // this language will be used as a fallback when a translation isn't found in the current language
    this._translateService.setDefaultLang('en');
    // the lang to use, if the lang isn't available, it will use the current loader to get them
    this._translateService.use(locale);
  }

  static forRoot(conf?: IConfig): ModuleWithProviders<NgxImModule> {
    return {
      ngModule: NgxImModule,
      providers: [{ provide: APP_CONFIG, useValue: conf }]
    }
  }

  private async ngOnInit() {
    Object.keys(IM_LOCALES).forEach(language => {
      if (IM_LOCALES.hasOwnProperty(language)) {
        this._translateService.setTranslation(language, IM_LOCALES[language], true);
      }
    });
  }
}
