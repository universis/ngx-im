import { Injectable } from "@angular/core";
import { ChannelType } from "../interfaces/MessagingChannelTypes";

export type ImPreferences = {
  sidebar?: boolean;
  sidebarRight?: boolean;
} & {
  [key in ChannelType]?: boolean;
}

type PreferenceKey = keyof ImPreferences;
type PreferenceValue = ImPreferences[PreferenceKey];

@Injectable({
  providedIn: 'root'
})
export class ImPreferencesService {
  constructor() { }

  public getPreferences(): any {
    return JSON.parse(localStorage.getItem("im.preferences") || "{}") as ImPreferences;
  }

  public setPreferences(preferences: ImPreferences): void {
    localStorage.setItem("im.preferences", JSON.stringify(preferences));
  }

  public getPreference(key: PreferenceKey): any {
    const preferences = this.getPreferences();
    return preferences[key];
  }

  public setPreference(key: PreferenceKey, value: PreferenceValue): void {
    const preferences = this.getPreferences();
    preferences[key] = value;
    this.setPreferences(preferences);
  }

  public clear(): void {
    localStorage.removeItem("im.preferences");
  }
}