import { Injectable } from "@angular/core";
import { ModalService } from "@universis/common";
import { ChannelType } from "../interfaces/MessagingChannelTypes";
import { AddChannelRouterComponent } from "../components/modals/add-channel-router/add-channel-router.component";
import * as pluralize from "pluralize";

@Injectable({
  providedIn: 'root'
})
export class AddChannelService {
  constructor(
    private _modalService: ModalService
  ) { }

  openModal(model: ChannelType, callback?: any) {
    try {
      this._modalService.openModalComponent(AddChannelRouterComponent, {
        class: 'modal-lg',
        ignoreBackdropClick: false,
        closeOnSubmit: true,
        initialState: {
          data: {
            model: pluralize(model),
            callback: callback
          }
        }
      });
    } catch (err) {
      console.log(err);
      // this._errorService.showError(err, {
      //   continueLink: '.'
      // });
    }
  }
}