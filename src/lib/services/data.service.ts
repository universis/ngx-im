import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { IChannel } from '../interfaces/IChannel';
import { IMessage } from '../interfaces/IMessage';
import { IAccount } from '../interfaces/IAccount';
import { IConfig } from '../interfaces/IConfig';
import { ImPreferencesService } from './im-preferences.service';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private _openSidebar: BehaviorSubject<boolean>;
  readonly _openSidebar$: Observable<boolean>;

  private _openSidebarRight: BehaviorSubject<boolean>;
  readonly _openSidebarRight$: Observable<boolean>;

  private readonly _selectedChannel = new BehaviorSubject<IChannel | null>(null);
  readonly _selectedChannel$ = this._selectedChannel.asObservable();

  private readonly _channels = new BehaviorSubject<IChannel[]>([]);
  readonly _channels$ = this._channels.asObservable();

  private readonly _messages = new BehaviorSubject<IMessage[]>([]);
  readonly _messages$ = this._messages.asObservable();

  private readonly _replies = new BehaviorSubject<IMessage[]>([]);
  readonly _replies$ = this._replies.asObservable();

  private readonly _error = new BehaviorSubject<string | null>(null);
  readonly _error$ = this._error.asObservable();

  private readonly _scrollToBottom = new BehaviorSubject<boolean>(true);
  readonly _scrollToBottom$ = this._scrollToBottom.asObservable();

  private readonly _scrollToBottomThread = new BehaviorSubject<boolean>(true);
  readonly _scrollToBottomThread$ = this._scrollToBottomThread.asObservable();

  private readonly _disableScrollToBottom = new BehaviorSubject<boolean>(false);
  readonly _disableScrollToBottom$ = this._disableScrollToBottom.asObservable();

  private readonly _disableScrollToBottomThread = new BehaviorSubject<boolean>(false);
  readonly _disableScrollToBottomThread$ = this._disableScrollToBottomThread.asObservable();

  private readonly _messagesCountChannel = new BehaviorSubject<number>(0);
  readonly _messagesCountChannel$ = this._messagesCountChannel.asObservable();

  private readonly _totalRepliesMessage = new BehaviorSubject<number>(0);
  readonly _totalRepliesMessage$ = this._totalRepliesMessage.asObservable();

  private readonly _account = new BehaviorSubject<IAccount | null>(null);
  readonly _account$ = this._account.asObservable();

  private _config: IConfig | null = null;

  constructor(
    private _imPreferencesService: ImPreferencesService
  ) {
    this._openSidebar = new BehaviorSubject<boolean>(
      this._imPreferencesService.getPreference("sidebar")
    );
    this._openSidebar$ = this._openSidebar.asObservable();

    this._openSidebarRight = new BehaviorSubject<boolean>(
      this._imPreferencesService.getPreference("sidebarRight")
    );
    this._openSidebarRight$ = this._openSidebarRight.asObservable();
  }

  get openSidebar(): boolean {
    return this._openSidebar.getValue();
  }

  set openSidebar(val: boolean) {
    this._openSidebar.next(!val);
  }

  get openSidebarRight(): boolean {
    return this._openSidebarRight.getValue();
  }

  set openSidebarRight(val: boolean) {
    this._openSidebarRight.next(!val);
  }

  get selectedChannel(): IChannel | null {
    return this._selectedChannel.getValue();
  }

  set selectedChannel(val: IChannel | null) {
    this._selectedChannel.next(val);
  }

  get channels(): IChannel[] {
    return this._channels.getValue();
  }

  set channels(channels: IChannel[]) {
    this._channels.next(channels);
  }

  get messages(): IMessage[] {
    return this._messages.getValue();
  }

  set messages(messages: IMessage[]) {
    this._messages.next(messages);
  }

  get error(): string | null {
    return this._error.getValue();
  }

  set error(val: string | null) {
    this._error.next(val);
  }

  get scrollToBottom(): boolean {
    return this._scrollToBottom.getValue();
  }

  set scrollToBottom(val: boolean) {
    this._scrollToBottom.next(val);
  }

  get scrollToBottomThread(): boolean {
    return this._scrollToBottomThread.getValue();
  }

  set scrollToBottomThread(val: boolean) {
    this._scrollToBottomThread.next(val);
  }

  get disableScrollToBottom(): boolean {
    return this._disableScrollToBottom.getValue();
  }

  set disableScrollToBottom(val: boolean) {
    this._disableScrollToBottom.next(val);
  }

  get disableScrollToBottomThread(): boolean {
    return this._disableScrollToBottomThread.getValue();
  }

  set disableScrollToBottomThread(val: boolean) {
    this._disableScrollToBottomThread.next(val);
  }

  get replies(): IMessage[] {
    return this._replies.getValue();
  }

  set replies(messages: IMessage[]) {
    this._replies.next(messages);
  }

  get messagesCountChannel(): number {
    return this._messagesCountChannel.getValue();
  }

  set messagesCountChannel(val: number) {
    this._messagesCountChannel.next(val);
  }

  get totalRepliesMessage(): number {
    return this._totalRepliesMessage.getValue();
  }

  set totalRepliesMessage(val: number) {
    this._totalRepliesMessage.next(val);
  }

  get account(): IAccount | null {
    return this._account.getValue();
  }

  set account(val: IAccount | null) {
    this._account.next(val);
  }

  get config(): IConfig | null {
    return this._config;
  }

  set config(val: IConfig | null) {
    this._config = Object.assign({}, val)
  }
}
