import { Injectable, Injector } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { AppEventService, ServerEvent, ServerEventSubscriber } from '@universis/common';

@Injectable()
export class InstantMessageSubscriber implements ServerEventSubscriber {
  constructor(
    private _context: AngularDataContext,
    private _appEvent: AppEventService
  ) { }

  subscribe(event: any | ServerEvent): void {
    if (event == null) {
      return;
    }

    if (event.entityType === 'MessagingChannel' || event.entityType === 'InstantMessage') {
      switch (event.state) {
        case 1:
          this._appEvent.add.next(event);
          break;
        case 2:
          this._appEvent.change.next(event);
          break;
        case 4:
          this._appEvent.remove.next(event);
          break;
      }
    }

    if (event.entityType === 'InstantMessageReaction') {
      this._appEvent.change.next(event);
    }
  }
}
