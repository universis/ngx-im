import { InjectionToken } from "@angular/core";
import { IConfig } from "./interfaces/IConfig";

export const APP_CONFIG = new InjectionToken<IConfig>('APP_CONFIG_PARAMS');