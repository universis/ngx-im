import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ISidebarRightItemNav, ISidebarRightNav } from '../../interfaces/ISidebarNav';
import { DataService } from '../../services/data.service';
import * as _ from 'lodash';
import { IAccount, IGroup } from '../../interfaces/IAccount';
import { IMember } from '../../interfaces/IMember';
import { TranslateService } from '@ngx-translate/core';
import { IAttachment } from '../../interfaces/IAttachment';

// TODO: Figure out how to display this correctly at the right side on smaller screens

@Component({
  selector: 'im-sidebar-right',
  templateUrl: './sidebar-right.component.html',
  styleUrls: ['./sidebar-right.component.scss']
})
export class SidebarRightComponent {
  @Output() openViewMemberModal = new EventEmitter<IMember>();
  @Output() openViewAttachmentModal = new EventEmitter<IAttachment>();

  @Input() loading!: boolean;

  members!: IMember[];
  menusInit: ISidebarRightNav[] = [];
  menus: ISidebarRightNav[] = [];
  searchValue: string = '';
  init: boolean = true;
  isAdmin: boolean = false;
  openSidebarRight: boolean = false;
  currentUser!: IMember;

  private _unsubscribeAll: Subject<void>;

  constructor(
    private _dataService: DataService,
    private _translateService: TranslateService
  ) {
    this._unsubscribeAll = new Subject();
  }

  ngOnInit() {
    this._dataService._account$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(res => {
        const data: IAccount | any = { ...res };
        const { imAccount } = data;
        this.isAdmin = !_.isEmpty(_.find(data.groups, (x: IGroup) => x.name === 'Administrators'))
        this.currentUser = imAccount;
      });

    this._dataService._openSidebarRight$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(val => this.openSidebarRight = val);

    this._dataService._selectedChannel$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(val => {
        this.menusInit =
          [{
            title: this._translateService.instant('IM.Members'),
            type: 'members',
            collapse: this.menus[0]?.collapse ? this.menus[0].collapse : this.init,
            children: val?.members
              .map(member =>
                (member))
              || []
          },
          {
            title: this._translateService.instant('IM.Media'),
            type: 'media',
            collapse: this.menus[1]?.collapse ? this.menus[1].collapse : false,
            children: []
          }];
        this.onSearchChange(this.searchValue);
      });
  }

  childIsMember(child: ISidebarRightItemNav): child is IMember {
    if ('name' in child) {
      return true;
    }
    return false;
  }

  childIsAttachment(child: ISidebarRightItemNav): child is IAttachment {
    if ('attachmentType' in child) {
      return true;
    }
    return false;
  }

  viewMember(member: IMember) {
    this.openViewMemberModal.emit(member);
  }

  viewAttachment(attachment: IAttachment) {
    this.openViewAttachmentModal.emit(attachment);
  }

  checkMemberName(sideNavItem: ISidebarRightItemNav): boolean {
    // type of IMember
    if (this.childIsMember(sideNavItem)) {
      const name = sideNavItem.name || "";
      const isIncluded = name.toLowerCase().indexOf(this.searchValue) > -1;
      return isIncluded;
    }
    return false;
  }

  onSearchChange(event: any) {
    this.searchValue = (typeof event === 'string') ? event : event.target.value.toLowerCase().trim();
    this.menus = this
      .menusInit
      .map(x => ({
        ...x,
        children: x.children.filter(y => this.checkMemberName(y))
      }))
    this.init = false;
  }

  collapseMenus(index: number) {
    this.menus[index].collapse = !this.menus[index].collapse
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}
