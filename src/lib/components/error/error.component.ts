import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { DataService } from '../../services/data.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'im-error',
  // templateUrl: './error.component.html',
  template: ``,
  styleUrls: ['./error.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ErrorComponent implements OnInit, OnDestroy {
  dismissible: boolean = true;
  dismissOnTimeout: number = 5000;
  alertsDismiss: string[] = [];

  private _unsubscribeAll: Subject<void>;

  constructor(
    private _dataService: DataService,
    private _toastrService: ToastrService
  ) {
    this._unsubscribeAll = new Subject();
  }

  ngOnInit(): void {
    this._dataService._error$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(val => {
        // if (val) this.alertsDismiss.push(val)
        if (val) this._toastrService.error(val, 'Error');
      });
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

}
