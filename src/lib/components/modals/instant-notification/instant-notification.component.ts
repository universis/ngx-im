import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { IChannel, IChannelHide } from '../../../interfaces/IChannel';
import { DataService } from '../../../services/data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'im-instant-notification',
  templateUrl: './instant-notification.component.html',
  styleUrls: ['./instant-notification.component.scss']
})
export class InstantNotificationComponent implements OnInit, OnDestroy {
  @ViewChild('instantNotificationModal') instantNotificationModal!: ModalDirective;

  room!: IChannel;
  channels: IChannel[] = [];
  loading: boolean = false;

  private _unsubscribeAll: Subject<void>;

  constructor(
    private _context: AngularDataContext,
    private _dataService: DataService,
    private _router: Router
  ) {
    this._unsubscribeAll = new Subject();
  }

  ngOnInit(): void {
    this._dataService._selectedChannel$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(val => {
        if (val) {
          this.room = val;
        }
      });

    this._dataService._channels$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(val => {
        if (val) {
          this.channels = val;
        }
      });
  }

  openInstantNotificationModal() {
    this.instantNotificationModal?.show();
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}
