import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { AngularDataContext } from '@themost/angular';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { IChannel, IChannelPost, ICourseClassChannelPost, IStudentThesisChannelPost } from '../../../interfaces/IChannel';
import * as _ from 'lodash';
import { DataService } from '../../../services/data.service';
import { ChannelType, MessagingChannelTypes } from '../../../interfaces/MessagingChannelTypes';
import * as pluralize from 'pluralize';

@Component({
  selector: 'im-edit-channel',
  templateUrl: './edit-channel.component.html',
  styleUrls: ['./edit-channel.component.scss']
})
export class EditChannelComponent implements OnInit {
  @ViewChild('editChannelModal') editChannelModal!: ModalDirective;

  @Output() editNewChannel = new EventEmitter<{ oldChannel: IChannel; newChannel: IChannel }>();

  form!: FormGroup;
  submitted = false;
  keywords: string[] = [];
  loading: boolean = false;
  type: ChannelType | null = null;
  channel: IChannel | null = null;
  lockValues = [{ label: 'IM.ReadWrite', value: 0 }, { label: 'IM.ReadOnly', value: 1 }]
  // types = [{ label: 'Global', value: 1 }, { label: 'Educational', value: 2 }];

  constructor(
    private _formBuilder: FormBuilder,
    private _dataService: DataService,
    private _context: AngularDataContext
  ) { }

  ngOnInit(): void {
    this.form = this._formBuilder.group({
      headline: ['', [Validators.required]],
      readOnly: [this.lockValues[0].value, [Validators.required]],
      modelType: [MessagingChannelTypes.CHANNEL, [Validators.required]],
      keyword: [''],
    });
  }

  onSubmit(): void {
    this.submitted = true;

    if (this.form?.invalid) {
      return;
    }

    const { headline, readOnly } = this.form?.value;

    const data = {
      ...this.channel,
      headline,
      alternateName: headline,
      readOnly: parseInt(readOnly),
      keywords: this.keywords,
    }

    const extra = {
    }

    this.saveApiData(data as any);
  }


  onReset(): void {
    this.editChannelModal.hide();
    this.submitted = false;
    this.form.get('headline')?.setValue('');
    this.form.get('readOnly')?.setValue(this.lockValues[0].value);
    this.form.get('keyword')?.setValue('');
    this.form.get('modelType')?.setValue(MessagingChannelTypes.CHANNEL);
    this.type = null;
    this.channel = null;
    this.keywords = [];
  }

  openEditChannelModal(type: ChannelType, channel: IChannel | null) {
    if (!channel) return;
    const { headline, readOnly, keywords, type: channelType } = channel;
    this.form.get('headline')?.setValue(headline);
    this.form.get('readOnly')?.setValue(readOnly == true ? this.lockValues[1].value : this.lockValues[0].value);
    if (keywords) this.keywords = [...keywords];
    this.type = type;
    this.channel = channel;
    this.editChannelModal.show();
  }

  get f(): { [key: string]: AbstractControl } {
    return this.form.controls;
  }

  appendKeyword() {
    const keyword = this.form?.get('keyword')?.value;
    const alreadyIncluded = this.keywords.find(x => x === keyword.trim());
    if (!alreadyIncluded && keyword.trim()) {
      this.keywords.push(keyword.trim());
      this.form?.get('keyword')?.setValue('');
    }
  }

  removeKeyword(keyword: string) {
    this.keywords = this.keywords.filter(x => x !== keyword);
  }

  urlValidator(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      let validUrl = true;
      const value = control.value;
      try {
        if (value) new URL(value);
      } catch {
        validUrl = false;
      }

      return validUrl ? null : { invalidUrl: true };
    }
  }

  isCourseClassChannel(data: any): data is ICourseClassChannelPost {
    return data.courseClass !== undefined;
  }

  isStudentThesisChannel(data: any): data is IStudentThesisChannelPost {
    return data.studentThesis !== undefined;
  }

  saveApiData(data: IChannelPost) {
    if (!this.type) return;
    this.loading = true;
    this._context.model(pluralize(this.type)) // pluralize model name
      .save(data)
      .then(results => {
        const channel: IChannel = results;
        this.editNewChannel.emit({ oldChannel: this.channel as any, newChannel: { ...channel, maintainer: this.channel?.maintainer } });
        this.onReset();
      })
      .catch(error => this._dataService.error = error.message)
      .finally(() => this.loading = false)
  }
}

