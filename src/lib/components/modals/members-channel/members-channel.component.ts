import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild, ViewEncapsulation } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { IChannel } from '../../../interfaces/IChannel';
import { IMember } from '../../../interfaces/IMember';
import { DataService } from '../../../services/data.service';
import { AddEditMemberChannelComponent } from '../add-edit-member-channel/add-edit-member-channel.component';
import * as _ from 'lodash';
import { IAccount } from '../../../interfaces/IAccount';
import { MessagingChannelTypes } from '../../../interfaces/MessagingChannelTypes';
import * as pluralize from 'pluralize';
import { ToastrService } from 'ngx-toastr';
import { LoadingService } from '@universis/common';

const MAX_PAGINATION_SIZE = 5;
const DELETE_CODE = 4;

@Component({
  selector: 'im-members-channel',
  templateUrl: './members-channel.component.html',
  styleUrls: ['./members-channel.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MembersChannelComponent implements OnInit, OnDestroy {
  @ViewChild('membersModal') membersModal!: ModalDirective;
  @ViewChild(AddEditMemberChannelComponent) addEditMemberChannelModal!: AddEditMemberChannelComponent;

  @Output() editNewChannel = new EventEmitter<{ oldChannel: IChannel; newChannel: IChannel }>();

  @Input() openViewMemberModal!: EventEmitter<IMember>;

  room!: IChannel;
  channelTableMembers: IMember[] = [];
  channelMembers: IMember[] = [];
  newMembers: IMember[] = [];
  // editMember!: IMember;
  maxSize: number = MAX_PAGINATION_SIZE;
  loading: boolean = false;
  currentUser!: IMember;
  canPopulate: boolean = false;

  private _unsubscribeAll: Subject<void>;

  constructor(
    private _context: AngularDataContext,
    private _dataService: DataService,
    private _loadingService: LoadingService,
    private _toastrService: ToastrService
  ) {
    this._unsubscribeAll = new Subject();
  }

  ngOnInit(): void {
    this._dataService.
      _account$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(res => {
        const data: IAccount | any = { ...res };
        const { imAccount } = data;
        this.currentUser = imAccount;
      });

    this._dataService.
      _selectedChannel$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(val => {
        if (val) {
          this.room = val;

          this.canPopulate = this.room.additionalType === MessagingChannelTypes.COURSE_CLASS
            || this.room.additionalType === MessagingChannelTypes.STUDENT_THESIS;

          const members = val.members || [];
          this.getData(members);
        }
      });
  }

  getData(members: IMember[]) {
    this.channelMembers = members.map(x => ({
      ...x,
      class: this.getBadgeClass(x.additionalType.toLowerCase())
    }));
    this.channelTableMembers = [...this.channelMembers];
    this.channelTableMembers = this.channelTableMembers.slice();
  }

  getBadgeClass(additionalType: string) {
    switch (additionalType) {
      case 'user':
        return 'badge badge-success';
      case 'account':
        return 'badge badge-secondary';
      default:
        return '';
    }
  }

  openEditModal() {
    this.channelTableMembers = [...this.channelMembers];
    this.newMembers = [];
    this.membersModal.show();
  }

  openAddMemberModal() {
    this.addEditMemberChannelModal.openAddMembersModal();
  }

  newUser(user: IMember) {
    this.newMembers.push({
      ...user,
    });
    this.newMembers = this.newMembers.slice();
  }

  populateMembers() {
    if (!this.canPopulate) return;
    this._loadingService.showLoading();
    this._context.model(`${pluralize(this.room.additionalType)}/${this.room.id}/PopulateMembers`)
      .save({})
      .then((members) => {
        this._toastrService.success('Members populated successfully');
        this.getData(members.value);
      })
      .catch((err) => {
        this._dataService.error = err.message;
      })
      .finally(() => this._loadingService.hideLoading());
  }

  isDisabled() {
    const totalUsers = this.channelTableMembers.length + this.newMembers.length;
    return totalUsers === 0 || totalUsers === 1;
  }

  close() {
    this.membersModal.hide();
  }

  save() {
    const members =
      _.uniqBy<IMember>([
        ...(
          this.channelMembers.map(({ ['class']: _, ...keep }) => keep)
            .map(x => this.channelTableMembers.find(y => y.id === x.id) || this.newMembers.find(z => z.name === x.name)
              ? x
              : { ...x, $state: DELETE_CODE }
            )
        ),
        ...this.newMembers
      ], 'name');
    // also add current user as member by default
    const savedMembers = members.find(x => x.name === this.currentUser?.name) ? [...members] : [...members, { name: this.currentUser?.name }];
    this.saveApiData(savedMembers as any);
  }

  removeEmit(user: IMember, type: 'current' | 'new') {
    switch (type) {
      case 'current':
        this.channelTableMembers = this.channelTableMembers.filter(x => x !== user);
        return;
      case 'new':
        this.newMembers = this.newMembers.filter(x => x !== user);
        return;
      default:
        return;
    }

  }

  async saveApiData(data: IMember[]) {
    this.loading = true;
    try {
      const memberCount = data.filter(y => !y.$state).length;
      // refetch channel
      let model = this.room.additionalType ?? 'MessagingChannel';
      const channel: IChannel = await this._context.model(pluralize(model)).where('id').equal(this.room.id).getItem()
      if (!channel) throw new Error('Channel not found');
      const members: IMember[] = await this.saveChannelMembers(data);
      const newMembers = members
        .filter(y => !y.$state)
        .map(x => ({
          ...x,
          additionalType: x.additionalType ?? 'Account',
        }));
      this.saveChannel({
        ...channel,
        members: newMembers
      });
      this.newMembers = [];
      this.close();
    } catch (err: any) {
      this._dataService.error = err.message
    } finally {
      this.loading = false
    }
  }

  saveChannel(channel: IChannel) {
    this.editNewChannel.emit({ oldChannel: this.room, newChannel: channel });
  }

  async saveChannelMembers(data: IMember[]) {
    if (!this.room) return [];
    const result = await this._context
      .model(`MessagingChannels/${this.room?.id}/Members`)
      .save(data);
    if (Array.isArray(result.value))
      return result.value;
    return [];
  }

  editEmit(user: IMember) {
    this.addEditMemberChannelModal.openEditMembersModal(user);
  }

  editUser(data: { oldUser: IMember; newUser: IMember }) {
    const { oldUser, newUser } = data;
    this.newMembers = this.newMembers.map(x => x.name === oldUser.name ? newUser : x);
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

}
