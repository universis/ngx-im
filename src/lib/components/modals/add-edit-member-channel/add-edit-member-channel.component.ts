import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { IMember } from '../../../interfaces/IMember';

@Component({
  selector: 'im-add-edit-member-channel',
  templateUrl: './add-edit-member-channel.component.html',
  styleUrls: ['./add-edit-member-channel.component.scss'],
})
export class AddEditMemberChannelComponent implements OnInit {
  @ViewChild('addMemberChannelModal') addEditMemberChannelModal!: ModalDirective;

  @Output() newUser = new EventEmitter<IMember>();
  @Output() editUser = new EventEmitter<{ oldUser: IMember; newUser: IMember }>();

  @Input() members: IMember[] = [];
  @Input() newMembers: IMember[] = [];

  currentUser: IMember | null = null;
  form!: FormGroup;
  submitted = false;

  constructor(
    private _formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {
    this.form = this._formBuilder.group({
      name: ['', [Validators.required, this.createUniqueName()]],
    });
  }

  createUniqueName(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      const value = control.value;

      if (!value) {
        return null;
      }

      const userFound = [...this.members, ...this.newMembers].find(x => x.name.trim() === value.trim());
      return userFound ? { notUniqueName: true } : null
    }
  }

  openAddMembersModal() {
    this.addEditMemberChannelModal.show()
  }

  openEditMembersModal(editUser: IMember) {
    const { name } = editUser;
    this.currentUser = editUser;
    this.form.get('name')?.setValue(name);
    this.addEditMemberChannelModal.show()
  }

  get f(): { [key: string]: AbstractControl } {
    return this.form.controls;
  }

  onSubmit(): void {
    this.submitted = true;

    if (this.form.invalid) {
      return;
    }

    const { name } = this.form.value;
    const user = { name };
    this.currentUser ? this.editUser.emit({ oldUser: this.currentUser, newUser: user as any }) : this.newUser.emit(user as any)
    this.onReset()
  }

  onReset(): void {
    this.submitted = false;
    this.currentUser = null;
    this.form.get('name')?.setValue('')
    this.addEditMemberChannelModal.hide();
  }

}
