import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { IChannel, IChannelHide } from '../../../interfaces/IChannel';
import { DataService } from '../../../services/data.service';
import { Router } from '@angular/router';
import * as pluralize from 'pluralize';

@Component({
  selector: 'im-hide-channel',
  templateUrl: './hide-channel.component.html',
  styleUrls: ['./hide-channel.component.scss']
})
export class HideChannelComponent implements OnInit, OnDestroy {
  @ViewChild('hideChannel') hideChannel!: ModalDirective;

  room: IChannel | null = null;
  channels: IChannel[] = [];
  loading: boolean = false;

  private _unsubscribeAll: Subject<void>;

  constructor(
    private _context: AngularDataContext,
    private _dataService: DataService,
    private _router: Router
  ) {
    this._unsubscribeAll = new Subject();
  }

  ngOnInit(): void {
    this._dataService._selectedChannel$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(val => {
        if (val) this.room = val;
      });

    this._dataService._channels$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(val => {
        if (val) this.channels = val;
      });
  }

  openHideChannelModal() {
    this.hideChannel.show();
  }

  hide() {
    const id = this.room?.id;
    const data = {
      id,
      isDisabled: true
    }
    if (id) this.hideApiData(data as any);
  }

  hideApiData(data: IChannelHide) {
    this.loading = true;
    let model = this.room?.additionalType ?? 'MessagingChannel';
    this._context
      .model(pluralize(model))
      .save(data)
      .then(() => {
        const { id } = data;
        const url = this._router.url;
        this._dataService.selectedChannel = null;
        this._dataService.channels = [...this.channels.filter(x => x.id !== id)];
        this._router.navigate(url.split('/').slice(0, -1))
          .finally(() => this.hideChannel.hide())
      })
      .catch(error => this._dataService.error = error.message)
      .finally(() => this.loading = false);
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}
