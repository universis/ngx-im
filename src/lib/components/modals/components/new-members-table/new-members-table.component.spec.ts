import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewMembersTableComponent } from './new-members-table.component';

describe('NewMembersTableComponent', () => {
  let component: NewMembersTableComponent;
  let fixture: ComponentFixture<NewMembersTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewMembersTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewMembersTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
