import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { IMember } from '../../../../interfaces/IMember';
import { IAccount } from '../../../../interfaces/IAccount';
import { DataService } from '../../../../services/data.service';
import { AngularDataContext } from '@themost/angular';
@Component({
  selector: 'im-current-members-table',
  templateUrl: './current-members-table.component.html',
  styleUrls: ['./current-members-table.component.scss']
})
export class CurrentMembersTableComponent implements OnInit, OnChanges, OnDestroy {
  @Output() removeEmit = new EventEmitter<IMember>();

  @Input() members: IMember[] = [];
  @Input() maxSize: number = 0;
  @Input() openViewMemberModal!: EventEmitter<IMember>;

  currentUser!: IMember;
  currentPage: number = 1;
  totalItems: number = 0;

  private _unsubscribeAll: Subject<void>;

  constructor(
    private _dataService: DataService,
    private _context: AngularDataContext
  ) {
    this._unsubscribeAll = new Subject();
  }

  ngOnInit(): void {
    this._dataService._account$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(res => {
        const data: IAccount | any = { ...res };
        const { imAccount } = data;
        this.currentUser = imAccount;
      });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.members && changes.members.currentValue) {
      if (this.members.length > 0 && this.currentPage > 1 && !this.members.slice((this.currentPage - 1) * this.maxSize, this.currentPage * this.maxSize).length) {
        this.currentPage = this.currentPage - 1;
      }
      this.totalItems = this.members.length;
    }
  }

  pageChanged(event: any) {
    this.currentPage = event.page;
  }

  remove(user: IMember) {
    this.removeEmit.emit(user);
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  async getUser(imAccount: IMember | null) {
    if (!imAccount) return null;
    const user = await this._context.model('Users')
      .where('id').equal(imAccount.account)
      .expand('imAccount')
      .getItem();

    return user;
  }

  viewMember(member: IMember) {
    this.openViewMemberModal.emit(member);
  }

}
