import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { IChannel } from '../../../interfaces/IChannel';
import { IMember } from '../../../interfaces/IMember';
import { IMessage } from '../../../interfaces/IMessage';
import { IReaction, IReactionPost } from '../../../interfaces/IReaction';
import { DataService } from '../../../services/data.service';
import { IAccount } from '../../../interfaces/IAccount';
import { AppEventService } from '@universis/common';

const PAGE = 20;
@Component({
  selector: 'im-content-thread',
  templateUrl: './content-thread.component.html',
  styleUrls: ['./content-thread.component.scss']
})
export class ContentThreadComponent implements OnInit, OnDestroy {
  @ViewChild('threadMessages') threadMessages!: ModalDirective;

  user!: IMember;
  message!: IMessage;
  messages: IMessage[] = [];
  messagesChannel: IMessage[] = [];
  index: number = 0;
  disabled: boolean = false;
  channel!: IChannel;
  loadingMessages: boolean = false;
  loadingInitMessages: boolean = false;
  totalRepliesMessages: number = 0;

  private _unsubscribeAll: Subject<void>;

  constructor(
    private _context: AngularDataContext,
    private _dataService: DataService,
    private _appEvent: AppEventService
  ) {
    this._unsubscribeAll = new Subject();
  }

  ngOnInit(): void {
    this._appEvent.added.subscribe((event: any) => {
      if (event.entityType === 'InstantMessage') {
        const entity = event.entity;
        if (entity.recipient === this.channel.id) {
          this.loadNewMessage(entity.identifier);
        }
      }
    });

    this._appEvent.changed.subscribe((event: any) => {
      if (event.entityType === 'InstantMessageReaction') {
        const entity = event.entity;
        if (entity.instantMessage.recipient === this.channel.id) {
          this.reloadMessage(entity.instantMessage.identifier)
        }
      }
      
      if (event.entityType === 'InstantMessage') {
        const entity = event.entity;
        if (entity.recipient === this.channel.id) {
          this.reloadMessage(entity.identifier)
        }
      }
    });

    this._appEvent.removed.subscribe((event: any) => {
      if (event.entityType === 'InstantMessage') {
        const entity = event.entity;
        const newMessages = this.messages.filter(x => x.identifier !== entity.identifier);
        this._dataService.replies = [...newMessages];
        this._dataService.totalRepliesMessage = this.totalRepliesMessages ? this.totalRepliesMessages - 1 : 0;
      }
    });

    this._dataService._account$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(res => {
        const data: IAccount | any = { ...res };
        const { imAccount } = data;
        this.user = imAccount;
      });

    this._dataService._selectedChannel$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(val => {
        if (val) {
          this.channel = val;
        }
      });

    this._dataService._replies$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(val => {
        this.messages = val || [];
      });

    this._dataService._messages$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(val => {
        this.messagesChannel = val || [];
      });

    this._dataService._totalRepliesMessage$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(res => {
        this.totalRepliesMessages = res;
      });
  }

  loadNewMessage(identifier: string) {
    this._context
      .model(`MessagingChannels/${this.channel?.id}/messages`)
      .where('parentInstantMessage').equal(this.message?.id)
      .and('identifier').equal(identifier)
      .expand('createdBy', 'sender', 'reactions', 'reactions($expand=user)')
      .take(1)
      .getItem()
      .then(res => {
        const message = res;
        if (message) {
          if (message.isReply) {
            this._dataService.replies = [message, ...this.messages];
            this._dataService.totalRepliesMessage += 1;
            if (!this._dataService.disableScrollToBottomThread) this._dataService.scrollToBottomThread = true;
          }
        }
      })
      .catch(error => this._dataService.error = error.message)
  }

  reloadMessage(identifier: string) {
    this._context
      .model(`MessagingChannels/${this.channel?.id}/messages`)
      .where('parentInstantMessage').equal(this.message?.id)
      .and('identifier').equal(identifier)
      .expand('createdBy', 'sender', 'reactions', 'reactions($expand=user)')
      .take(1)
      .getItem()
      .then(res => {
        const message = res;
        if (message) {
          if (message.isReply) {
            const newMessages = this.messages.map(x => x.id === message.id ? message : x);
            this._dataService.replies = [...newMessages];
          }
        }
      })
      .catch(error => this._dataService.error = error.message)
  }

  openThreadMessagesModal(message: IMessage) {
    this.message = message;
    this._dataService.scrollToBottomThread = true;
    this.index = 0;
    this.fetchMessageThread()
    this.threadMessages?.show()
  }

  fetchMessageThread() {
    if (this.index === 0) this.loadingInitMessages = true;
    this.loadingMessages = true;
    this._context
      .model(`MessagingChannels/${this.channel?.id}/messages`)
      .where('parentInstantMessage').equal(this.message?.id)
      .expand('createdBy', 'sender', 'reactions', 'reactions($expand=user)')
      .take(PAGE)
      .skip(PAGE * this.index)
      .orderByDescending('dateCreated')
      .getList()
      .then(results => {
        const { value, total } = results;
        if (this.index === 0) {
          this._dataService.totalRepliesMessage = total;
          this._dataService.replies = [];
        }
        this._dataService.replies = [...this.messages, ...value]
        this.disabled = value.length < PAGE;
        this.index++;
      })
      .catch(error => {
        this.disabled = true;
        this._dataService.error = error.message
      })
      .finally(() => {
        this.loadingInitMessages = false;
        this.loadingMessages = false
      });
  }

  fetchMore() {
    if (!this.disabled) this.fetchMessageThread()
  }

  reactionAction(event: { message: IMessage, index: number }) {
    const { message, index } = event;
    const reaction: IReactionPost = {
      reaction: index,
      instantMessage: message.id
    }
    const tempDisabled = this.disabled;
    this.disabled = true;
    this._context
      .model(`MessagingChannels/${this.channel?.id}/toggleReaction`)
      .save(reaction)
      .then((results: IReaction) => {
        const newMessage: IMessage | any =
          results ? {
            ...message,
            reactions: [
              ...message.reactions,
              {
                ...results,
                user: this.user
              }
            ].sort((x, y) => x.id - y.id)
          } : {
            ...message,
            reactions: message.reactions.filter(x => x.reaction !== index)
          }
        const newMessages = this.messages.map(x => x.id === newMessage.id ? newMessage : x);
        this._dataService.replies = [...newMessages];
      })
      .catch(error => this._dataService.error = error.message)
      .finally(() => this.disabled = tempDisabled);
  }

  deleteMessage(event: { message: IMessage }) {
    const { message } = event;
    const tempDisabled = this.disabled;
    this.disabled = true;
    this._context
      .model(`MessagingChannels/${this.channel?.id}/removeMessage`)
      .save(message)
      .then((results: { item: IMessage; children: IMessage }) => {
        const { item } = results;
        const data = [...this.messages].filter(x => x.id !== item.id)
        this._dataService.replies = [...data];
        this._dataService.totalRepliesMessage = this.totalRepliesMessages - 1;
        if (!data.length) this.updateMessageReply()
      })
      .finally(() => this.disabled = tempDisabled);
  }

  updateMessageReply() {
    this._context
      .model(`MessagingChannels/${this.channel?.id}/toggleMessageReply`)
      .save({
        ...this.message,
      })
      .then(message => {
        const index = this.messagesChannel.findIndex(x => x.id === message.id)
        if (index > -1) {
          // this.message?.hasReply = message.hasReply;
          if (this.message != null) {
            this.message.hasReply = message.hasReply;
          }
          this.messagesChannel[index].hasReply = message.hasReply;
          this._dataService.messages = [...this.messagesChannel];
        }
      })
  }
  styleObject = (): Object => ({ minHeight: `${this._dataService.config?.MIN_HEIGHT_REPLIES}`, maxHeight: `${this._dataService.config?.MAX_HEIGHT_REPLIES}` })

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}
