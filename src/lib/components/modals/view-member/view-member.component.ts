import { Component, ViewChild } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { IMember } from '../../../interfaces/IMember';
import { IAccount, IGroup } from '../../../interfaces/IAccount';
import { LoadingService } from '@universis/common';
import * as _ from 'lodash';

@Component({
  selector: 'im-view-member',
  templateUrl: './view-member.component.html',
  styleUrls: ['./view-member.component.scss']
})
export class ViewMemberComponent {
  @ViewChild('viewMember') viewMember!: ModalDirective;

  isStudent: boolean = false;
  isInstructor: boolean = false;
  isAdmin: boolean = false;
  student: any;
  instructor: any;
  user: IAccount | null = null;
  error: string | null = null;

  constructor(
    private _context: AngularDataContext,
    private _loadingService: LoadingService
  ) { }

  close() {
    this.isStudent = false;
    this.isInstructor = false;
    this.isAdmin = false;
    this.student = null;
    this.instructor = null;
    this.user = null;
    this.error = null;
    this.viewMember.hide();
  }

  async openModal(member: IMember) {
    this._loadingService.showLoading();
    this.viewMember.show();

    let user;
    try {
      user = await this._context.model(`InstantMessageAccounts/${member.id}/user`).asQueryable().getItem();
    } catch (err) {
      this.error = 'IM.Errors.MemberNotFound';
      this._loadingService.hideLoading();
      return;
    }

    if (!user) {
      this.error = 'IM.Errors.NoMemberInfo';
      this._loadingService.hideLoading();
      return;
    }

    this.user = user;
    this.isStudent = !_.isEmpty(_.find(this.user?.groups, (x: IGroup) => x.name === 'Students'));
    this.isInstructor = !_.isEmpty(_.find(this.user?.groups, (x: IGroup) => x.name === 'Instructors'));
    this.isAdmin = !_.isEmpty(_.find(this.user?.groups, (x: IGroup) => x.name === 'Administrators'));

    if (!this.isStudent && !this.isInstructor) {
      this.error = 'IM.Errors.NoMemberInfo';
      this._loadingService.hideLoading();
      return;
    }

    if (this.isStudent) {
      this._context.model(`InstantMessageAccounts/${member.id}/student`)
        .asQueryable()
        .getItem()
        .then((res: any) => {
          this.error = null;
          this.student = res;
        })
        .catch((err) => {
          this.error = 'IM.Errors.NoMemberInfo';
        })
        .finally(() => {
          this._loadingService.hideLoading();
        });
    }

    if (this.isInstructor) {
      this._context.model(`InstantMessageAccounts/${member.id}/instructor`)
        .asQueryable()
        .getItem()
        .then((res: any) => {
          this.error = null;
          this.instructor = res;
        })
        .catch((err) => {
          this.error = 'IM.Errors.NoMemberInfo';
        })
        .finally(() => {
          this._loadingService.hideLoading();
        });
    }
  }
}
