import { Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { AngularDataContext } from '@themost/angular';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { IChannel, IChannelPost } from '../../../interfaces/IChannel';
import { IMember } from '../../../interfaces/IMember';
import { DataService } from '../../../services/data.service';
import { AddEditMemberChannelComponent } from '../add-edit-member-channel/add-edit-member-channel.component';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { IAccount } from '../../../interfaces/IAccount';
import { ChannelType, MessagingChannelTypes } from '../../../interfaces/MessagingChannelTypes';

const MAX_PAGINATION_SIZE = 5;

@Component({
  selector: 'im-add-channel',
  templateUrl: './add-channel.component.html',
  styleUrls: ['./add-channel.component.scss']
})
export class AddChannelComponent implements OnInit, OnDestroy {
  @ViewChild('addChannelModal') addChannelModal!: ModalDirective;
  @ViewChild(AddEditMemberChannelComponent) addEditMemberChannelModal!: AddEditMemberChannelComponent;

  @Output() newChannel = new EventEmitter<IChannel>();

  form!: FormGroup;
  submitted = false;
  newMembers: IMember[] = [];
  maxSize: number = MAX_PAGINATION_SIZE;
  keywords: string[] = [];
  loading: boolean = false;
  type: ChannelType | null = null;
  user!: IMember;
  lockValues = [{ label: 'IM.ReadWrite', value: 0 }, { label: 'IM.ReadOnly', value: 1 }]
  MessagingChannelTypes = MessagingChannelTypes;
  // types = [{ label: 'Global', value: 1 }, { label: 'Educational', value: 2 }];

  private _unsubscribeAll: Subject<void>;

  constructor(
    private _formBuilder: FormBuilder,
    private _dataService: DataService,
    private _context: AngularDataContext
  ) {
    this._unsubscribeAll = new Subject();
  }

  ngOnInit(): void {
    this._dataService._account$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(res => {
        const data: IAccount | any = { ...res };
        const { imAccount } = data;
        this.user = imAccount;
      });

    this.form = this._formBuilder.group({
      headline: ['', [Validators.required]],
      readOnly: [this.lockValues[0].value, [Validators.required]],
      modelType: [MessagingChannelTypes.CHANNEL, [Validators.required]],
      keyword: [''],
    });

    this.form.get('modelType')?.valueChanges.subscribe(val => {
      this.type = val;
    })
  }

  onSubmit(): void {
    this.loading = true;
    this.submitted = true;
    if (this.form?.invalid) return;

    const { headline, readOnly, modelType } = this.form?.value;
    const data = {
      headline,
      alternateName: headline,
      readOnly: parseInt(readOnly),
      keywords: this.keywords,
      members: (this.newMembers.find(x => x.name === this.user?.name) ? [...this.newMembers] : [...this.newMembers, { name: this.user?.name }])
    }
    this.saveApiData(data as any, modelType as ChannelType)
  }

  saveApiData(data: any, modelType: ChannelType) {
    // check if modelType exists in enum MessagingChannelTypes
    const isType = Object.values(MessagingChannelTypes).includes(modelType);
    if (!isType) {
      this._dataService.error = 'Invalid channel type';
      this.loading = false;
      return;
    }

    this._context.model(modelType + "s") // pluralize model name
      .save(data)
      .then(results => {
        const channel: IChannel = { ...results, maintainer: this.user };
        this.newChannel.emit(channel);
        this.onReset();
      })
      .catch(error => this._dataService.error = error.message)
      .finally(() => {
        this.loading = false;
        this.submitted = false;
      });
  }

  onReset(): void {
    this.submitted = false;
    this.keywords = [];
    this.newMembers = [];
    this.type = null;
    this.form.get('headline')?.setValue('');
    this.form.get('readOnly')?.setValue(this.lockValues[0].value);
    this.form.get('keyword')?.setValue('');
    this.form.get('modelType')?.setValue(MessagingChannelTypes.CHANNEL)
    this.addChannelModal?.hide();
  }

  openAddChannelModal(type: ChannelType) {
    // if (type === MessagingChannelTypes.DIRECT) {
    //   this.form.get('name')?.setValidators([Validators.required])
    // } else {
    //   this.form.get('name')?.clearValidators()
    // }
    // this.form.get('name')?.updateValueAndValidity();
    this.type = type;
    this.form.get('modelType')?.setValue(type);
    this.addChannelModal?.show();
  }

  get f(): { [key: string]: AbstractControl } {
    return this.form.controls;
  }

  newUser(user: IMember) {
    this.newMembers.push({
      ...user,
    })
    this.newMembers = this.newMembers.slice()
  }

  removeEmit(user: IMember) {
    this.newMembers = this.newMembers.filter(x => x !== user)
  }

  openAddMemberModal() {
    this.addEditMemberChannelModal?.openAddMembersModal();
  }

  appendKeyword() {
    const keyword = this.form?.get('keyword')!.value;
    const alreadyIncluded = this.keywords.find(x => x === keyword.trim())
    if (!alreadyIncluded && keyword.trim()) {
      this.keywords.push(keyword.trim());
      this.form?.get('keyword')!.setValue('')
    }
  }

  removeKeyword(keyword: string) {
    this.keywords = this.keywords.filter(x => x !== keyword)
  }

  urlValidator(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      let validUrl = true;
      const value = control.value;
      try {
        if (value) new URL(value)
      } catch {
        validUrl = false;
      }

      return validUrl ? null : { invalidUrl: true };
    }
  }

  editEmit(user: IMember) {
    this.addEditMemberChannelModal?.openEditMembersModal(user)
  }

  editUser(data: { oldUser: IMember; newUser: IMember }) {
    const { oldUser, newUser } = data;
    this.newMembers = this.newMembers.map(x => x.name === oldUser.name ? newUser : x);
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

}
