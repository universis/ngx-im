import { AfterViewInit, Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { RouterModalOkCancel } from '@universis/common/routing';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingService, ModalService } from '@universis/common';
import { AdvancedFormComponent } from '@universis/forms';
import { AngularDataContext } from '@themost/angular';
import { Subject, Subscription, takeUntil } from 'rxjs';
import { ChannelType, MessagingChannelTypes } from '../../../interfaces/MessagingChannelTypes';
import { IChannel } from '../../../interfaces/IChannel';
import { DataService } from '../../../services/data.service';
import * as _ from 'lodash';
import * as pluralize from 'pluralize';
import { TranslateService } from '@ngx-translate/core';
import { IAccount, IGroup } from '../../../interfaces/IAccount';

@Component({
  selector: 'im-add-channel-router',
  templateUrl: './add-channel-router.component.html',
})
export class AddChannelRouterComponent extends RouterModalOkCancel implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('formComponent') formComponent!: AdvancedFormComponent;

  @Input() data: any;
  @Input() formEditName: any;

  lastError: any;
  loading: boolean = false;
  isInstructor: boolean = false;
  isAdmin: boolean = false;

  private _formLoadSubscription: Subscription | undefined;
  private _formChangeSubscription: Subscription | undefined;
  private _unsubscribeAll: Subject<void>;
  #refreshedForm: boolean = false;

  constructor(
    protected router: Router,
    protected activatedRoute: ActivatedRoute,
    private _modalService: ModalService,
    private _loadingService: LoadingService,
    private _context: AngularDataContext,
    private _dataService: DataService,
    private _translateService: TranslateService
  ) {
    super(router, activatedRoute);
    this._unsubscribeAll = new Subject();
    this._formLoadSubscription = new Subscription();
    this._formChangeSubscription = new Subscription();
    // set modal size
    this.modalClass = 'modal-lg';
    this.okButtonDisabled = true;
  }

  ngOnInit(): void {
    this.modalTitle = 'IM.Forms.' + pluralize.singular(this.data.model);
    this.formEditName = this.data.model + '/new';
  }

  ngAfterViewInit() {
    this._dataService._account$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(res => {
        const data: IAccount | any = { ...res };
        this.isAdmin = !_.isEmpty(_.find(data.groups, (x: IGroup) => x.name === 'Administrators'));
        this.isInstructor = !_.isEmpty(_.find(data.groups, (x: IGroup) => x.name === 'Instructors'));
      });

    this._formLoadSubscription = this.formComponent.form?.formLoad.subscribe(() => {
      if (this.formComponent.form?.config) {
        // find submit button
        const findButton = this.formComponent.form?.form?.components!.find((component) => {
          return component.type === 'button' && component.key === 'submit';
        });
        // hide button
        if (findButton) {
          findButton.hidden = true;
        }
        // if user is instructor,
        // we need to change the url field that is used to fetch specific scope data
        // such as course classes, student theses etc.
        if (this.isInstructor) {
          switch (pluralize.singular(this.data.model)) {
            case MessagingChannelTypes.COURSE_CLASS:
              // original url:
              // CourseClasses?$top={{limit}}&$skip={{skip}}&$filter=indexof(title, '{{encodeURIComponent(search||'')}}') ge 0

              // find courseClass field
              const findCourseClass = this.formComponent.form?.form?.components!.find((component) => {
                return component.type === 'select' && component.key === 'courseClass';
              });
              if (findCourseClass) {
                const url = findCourseClass.data.url;
                // find first occurance of "/CourseClasses"
                const index = url.indexOf('/CourseClasses');
                // return if not found
                if (index === -1) return;
                // replace "/CourseClasses" and everything after it with new endpoint
                let newUrl = url.substring(0, index) + '/Instructors/Me/classes?$top={{limit}}&$skip={{skip}}&$filter=indexof(title, \'{{encodeURIComponent(search||\'\')}}\') ge 0';
                findCourseClass.data.url = newUrl;
              }
              break;
            case MessagingChannelTypes.STUDENT_THESIS:
              // original url:
              // StudentTheses?$top={{limit}}&$skip={{skip}}&$filter=indexof(id, '{{encodeURIComponent(search||'')}}') ge 0 or indexof(thesis/name, '{{encodeURIComponent(search||'')}}') ge 0

              const findStudentThesis = this.formComponent.form?.form?.components!.find((component) => {
                return component.type === 'select' && component.key === 'studentThesis';
              });
              if (findStudentThesis) {
                const url = findStudentThesis.data.url;
                const index = url.indexOf('/StudentTheses');
                if (index === -1) return;
                let newUrl = url.substring(0, index) + '/Instructors/Me/thesisStudents?$top={{limit}}&$skip={{skip}}&$filter=indexof(id, \'{{encodeURIComponent(search||\'\')}}\') ge 0 or indexof(thesis/name, \'{{encodeURIComponent(search||\'\')}}\') ge 0';
                findStudentThesis.data.url = newUrl;
              }
              break;
          }
        }

        // refresh form
        if (!this.#refreshedForm) {
          this.formComponent.form.refresh?.emit({ form: this.formComponent.form?.form });
          this.#refreshedForm = true;
        }
      }
    });

    this.formComponent.formName = this.formEditName;
    this._formChangeSubscription = this.formComponent.form?.change.subscribe((event) => {
      if (Object.prototype.hasOwnProperty.call(event, 'isValid')) {
        // enable or disable button based on form status
        this.okButtonDisabled = !(<any>event).isValid;
      }
    });
  }

  async cancel() {
    // close
    if (this._modalService.modalRef) {
      return this._modalService.modalRef.hide();
    }
  }

  async ok() {
    this.lastError = null;

    if (!(this.formComponent.form && this.formComponent.form.formio)) {
      this.lastError = new Error(this._translateService.instant('IM.Errors.SomethingWentWrong'));
      return;
    }

    this._loadingService.showLoading();

    const isFormValid = this.formComponent.form.formio.checkValidity(this.formComponent.form.formio.data);
    if (!isFormValid) {
      this.lastError = new Error(this._translateService.instant('IM.Errors.InvalidForm'));
      this._loadingService.hideLoading();
      return;
    }

    // clone form data so we can edit its properties without affecting the form
    let formData = _.cloneDeep(this.formComponent.form.formio.data);
    delete formData.submit;
    let model = pluralize.singular(this.data.model);

    // check if modelType exists in enum MessagingChannelTypes
    const isType = Object.values(MessagingChannelTypes).includes(model as ChannelType);
    if (!isType) {
      this.lastError = new Error('Invalid channel type');
      this._loadingService.hideLoading();
      return;
    }

    this._context.model(this.data.model)
      .save(formData)
      .then(results => {
        const channel: IChannel = { ...results, maintainer: this._dataService.account?.imAccount };
        if (this.data.callback) this.data.callback(channel);
        this.cancel();
        // refetch channel
        // this._context.model(this.data.model)
        //   .select('id', 'alternateName', 'additionalType', 'maintainer', 'dateCreated', 'headline', 'isDisabled', 'readOnly', 'type')
        //   .where('id').equal(results.id)
        //   .expand('members')
        //   .getItem()
        //   .then(res => {
        //     this.data.updateChannels(res);
        //     this.cancel();
        //   })
        //   .catch(error => this.lastError = error)
      })
      .catch(error => this.lastError = error)
      .finally(() => {
        this._loadingService.hideLoading();
      });
  }

  ngOnDestroy(): void {
    if (this._formLoadSubscription) {
      this._formLoadSubscription.unsubscribe();
    }
    if (this._formChangeSubscription) {
      this._formChangeSubscription.unsubscribe();
    }
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}
