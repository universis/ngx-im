import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddChannelRouterComponent } from './add-channel-router.component';

describe('AddChannelRouterComponent', () => {
  let component: AddChannelRouterComponent;
  let fixture: ComponentFixture<AddChannelRouterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddChannelRouterComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AddChannelRouterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
