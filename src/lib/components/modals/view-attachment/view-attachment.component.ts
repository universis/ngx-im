import { Component, ViewChild } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { IAttachment } from '../../../interfaces/IAttachment';
import { IAccount } from '../../../interfaces/IAccount';
import { LoadingService } from '@universis/common';

@Component({
  selector: 'im-view-attachment',
  templateUrl: './view-attachment.component.html',
  styleUrls: ['./view-attachment.component.scss']
})
export class ViewAttachmentComponent {
  @ViewChild('viewAttachment') viewAttachment!: ModalDirective;

  account: IAccount | null = null;
  attachment: IAttachment | null = null;
  error: string | null = null;

  constructor(
    private _context: AngularDataContext,
    private _loadingService: LoadingService
  ) { }

  openModal(attachment: IAttachment) {
    this._loadingService.showLoading();

    this._context.model('Attachments')
      .where('id').equal(attachment.id)
      .getItem()
      .then((res: IAttachment) => {
        this.error = null;
        this.attachment = res;
        this.viewAttachment.show();
      })
      .catch((err) => {
        this.error = err.message;
      })
      .finally(() => {
        this._loadingService.hideLoading();
      });
  }
}
