import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'im-empty-content',
  templateUrl: './empty-content.component.html',
  styleUrls: ['./empty-content.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class EmptyContentComponent implements OnInit {
  constructor(
    private _dataService: DataService
  ) { }

  ngOnInit(): void {
    this._dataService.selectedChannel = null;
  }

}
