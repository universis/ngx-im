import { Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { IChannel } from '../../interfaces/IChannel';
import { MembersChannelComponent } from '../modals/members-channel/members-channel.component';
import { HideChannelComponent } from '../modals/hide-channel/hide-channel.component';
import { EditChannelComponent } from '../modals/edit-channel/edit-channel.component';
import { DataService } from '../../services/data.service';
import * as _ from 'lodash';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { IMember } from '../../interfaces/IMember';
import { IAccount, IGroup } from '../../interfaces/IAccount';
import { ImPreferencesService } from '../../services/im-preferences.service';
import { InstantNotificationComponent } from '../modals/instant-notification/instant-notification.component';
import { ChannelType, MessagingChannelTypes } from '../../interfaces/MessagingChannelTypes';
import { Router } from '@angular/router';
import { AddChannelService } from '../../services/add-channel.service';
import { AppEventService } from '@universis/common';

@Component({
  selector: 'im-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {
  @ViewChild(MembersChannelComponent) membersModal!: MembersChannelComponent;
  @ViewChild(EditChannelComponent) editModal!: EditChannelComponent;
  @ViewChild(HideChannelComponent) hideModal!: HideChannelComponent;
  @ViewChild(InstantNotificationComponent) instantNotificationModal!: InstantNotificationComponent;

  @Output() openViewMemberModal = new EventEmitter<IMember>();

  selectedRoom: IChannel | null = null;
  channels: IChannel[] = [];
  messagesCountChannel: number = 0;
  isAdmin: boolean = false;
  isInstructor: boolean = false;
  currentUser!: IMember;
  MessagingChannelTypes = MessagingChannelTypes;
  // types = [{ label: 'Global', value: 1 }, { label: 'Educational', value: 2 }];

  private _sideBarOpened: boolean = false;
  private _sideBarRightOpened: boolean = false;
  private _unsubscribeAll: Subject<void>;

  constructor(
    private _dataService: DataService,
    private _imPreferencesService: ImPreferencesService,
    private _router: Router,
    private _addChannelService: AddChannelService,
    private _appEvent: AppEventService
  ) {
    this._unsubscribeAll = new Subject();
  }

  ngOnInit(): void {
    this._appEvent.changed.subscribe((event: any) => {
      if (event.action === 'update-current-channel') {
        const entity = event.data.channel as IChannel;
        if (entity.id === this._dataService.selectedChannel?.id) {
          const newSelectedChannel = Object.assign({}, { ...this._dataService.selectedChannel, ...entity }, null) as IChannel;
          this._dataService.selectedChannel = { ...newSelectedChannel };
        }
      }
    });

    this._dataService._account$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(res => {
        const data: IAccount | any = { ...res };
        const { imAccount } = data;
        this.isAdmin = !_.isEmpty(_.find(data.groups, (x: IGroup) => x.name === 'Administrators'))
        this.isInstructor = !_.isEmpty(_.find(data.groups, (x: IGroup) => x.name === 'Instructors'))
        this.currentUser = imAccount;
      });

    this._dataService._openSidebar$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(val => this._sideBarOpened = val);

    this._dataService._openSidebarRight$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(val => this._sideBarRightOpened = val);

    this._dataService._selectedChannel$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(val => {
        this.selectedRoom = val ? Object.assign({}, { ...val, members: val.members.sort((x, _) => x.name === this.currentUser?.name ? -1 : 1) }, null) : null;
      });

    this._dataService._channels$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(val => this.channels = val);

    this._dataService._messagesCountChannel$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(val => this.messagesCountChannel = val);
  }

  onToggleSidebar() {
    this._dataService.openSidebar = this._sideBarOpened;
    this._imPreferencesService.setPreference("sidebar", this._sideBarOpened);
  }

  onToggleSidebarRight() {
    this._dataService.openSidebarRight = this._sideBarRightOpened;
    this._imPreferencesService.setPreference("sidebarRight", this._sideBarRightOpened);
  }

  membersChannel() {
    this.membersModal?.openEditModal();
  }

  hideChannel() {
    this.hideModal?.openHideChannelModal();
  }

  createInstantNotification() {
    this.instantNotificationModal.openInstantNotificationModal();
  }

  editChannel() {
    if (!this.selectedRoom) return;
    const type = this.selectedRoom.additionalType as ChannelType;
    const obj: IChannel = _.cloneDeep(this.selectedRoom);
    this.editModal.openEditChannelModal(type, obj);
  }

  editNewChannel(data: { oldChannel: IChannel; newChannel: IChannel }) {
    const { oldChannel, newChannel } = data;
    this._dataService.channels =
      this.channels.map((x) => x.id === oldChannel.id ? {
        ...newChannel,
        // isDirect: newChannel.members.length === 2 // convert to direct if members are exactly 2 (owner + 1 member)
      } : x)
    this._dataService.selectedChannel = newChannel;
  }

  getTooltip() {
    // return `${this.selectedRoom?.alternateName}, ${this.types.find(x => x.value === this.selectedRoom?.type)?.label}`;
    return `${this.selectedRoom?.alternateName}`;
  }

  openAddChannelModal(model: ChannelType) {
    this._addChannelService.openModal(model, this.updateChannelsState);
  }

  // use arrow function to have access to "this" keyword
  updateChannelsState = (newChannel: IChannel) => {
    try {
      const contains = this.channels.some(x => x.id === newChannel.id);
      if (contains) {
        this._dataService.channels = this.channels.map(x => x.id === newChannel.id
          ? Object.assign({}, { ...x, ...newChannel }, null)
          : x
        );
      } else {
        this._dataService.channels = [{ ...newChannel }, ...this.channels];
      }
      this._dataService.selectedChannel = newChannel;
      this._router.navigate(['im', 'room', newChannel.id]);
    } catch (err) {
      console.error(err);
    }
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}
