import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, Type, ViewEncapsulation } from '@angular/core';
import { AbstractControl, FormControl, FormGroup } from '@angular/forms';
import { AngularDataContext } from '@themost/angular';
import { IMember } from '../../../interfaces/IMember';
import { IMessage, IMessagePost } from '../../../interfaces/IMessage';
import { DataService } from '../../../services/data.service';
import { IAccount, IGroup } from '../../../interfaces/IAccount';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import * as _ from 'lodash';
import { IChannel } from '../../../interfaces/IChannel';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import * as pluralize from 'pluralize';

@Component({
  selector: 'im-content-actions',
  templateUrl: './content-actions.component.html',
  styleUrls: ['./content-actions.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ContentActionsComponent implements OnInit, OnDestroy {
  @Output() updateMessageReply = new EventEmitter();

  @Input() selectedId!: number;
  @Input() message: IMessage | null = null;
  @Input() isReplyChat: boolean = false;
  @Input() isInstantNotification: boolean = false;

  form!: FormGroup;
  sendButtonDisabled: boolean = false;
  messages: IMessage[] = [];
  user!: IMember;
  isAdmin: boolean = false;
  isMaintainer: boolean = false;
  userCanType: boolean = true;
  channel!: IChannel;
  totalRepliesMessages!: number;
  quillConfig = {
    toolbar: {
      container: [
        ['bold', 'italic', 'underline', 'strike'],
        [{ 'size': ['xsmall', 'small', 'medium', 'large', 'xlarge'] }],
        ['clean'],
      ],
    },
  }
  totalMessages!: number;

  private _unsubscribeAll: Subject<void>;

  constructor(
    private _context: AngularDataContext,
    private _dataService: DataService,
    private _translateService: TranslateService,
    private _toastrService: ToastrService
  ) {
    this._unsubscribeAll = new Subject();
  }

  ngOnInit(): void {
    this._dataService._account$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(res => {
        const data: IAccount | any = { ...res };
        const { imAccount } = data;
        this.isAdmin = !_.isEmpty(_.find(data.groups, (x: IGroup) => x.name === 'Administrators'));
        this.user = imAccount;
      });

    this._dataService._selectedChannel$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(val => {
        if (val) {
          this.channel = val;
          this.isMaintainer = this.channel.maintainer?.id === this._dataService.account?.id;
          this.userCanType = !this.channel.readOnly || this.isMaintainer || this.isAdmin;
        }
      });

    this.form = new FormGroup({
      input: new FormControl('')
    });

    this._dataService._messagesCountChannel$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(res => {
        this.totalMessages = res;
      });

    this._dataService._totalRepliesMessage$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(res => {
        this.totalRepliesMessages = res;
      });

    this.isReplyChat ? this._dataService._replies$.subscribe(val => this.messages = val) : this._dataService._messages$.subscribe(val => this.messages = val)
  }

  get f(): { [key: string]: AbstractControl } {
    return this.form?.controls;
  }

  strip(html: string) {
    return (new DOMParser().parseFromString(html ?? '', 'text/html').body.textContent || "").length;
  }

  postMessage() {
    this.sendButtonDisabled = true;

    // FIXME: this is not working
    // get input and check if it is empty
    const input = this.form?.get('input')?.value.trim();
    if (_.isEmpty(input)) {
      this.sendButtonDisabled = false;
      return;
    }

    const model = pluralize(this.channel.additionalType);
    if (!this.isInstantNotification) {
      const message: IMessagePost = this.message ?
        {
          body: input,
          isReply: true,
          parentInstantMessage: this.message.id,
          hasReply: false
        } :
        {
          body: input,
          isReply: false,
          hasReply: false,
        }

      this._context
        .model(`${model}/${this.selectedId}/messages`)
        .save(message)
        .then(results => {
          const message = {
            ...results,
            reactions: [],
            sender: { ...this.user },
          };
          if (this.message) {
            this._dataService.scrollToBottomThread = true;
            this._dataService.replies = [message, ...this.messages];
            this._dataService.totalRepliesMessage = this.totalRepliesMessages + 1;
            if (!this.message.hasReply) this.updateMessageReply.emit();
          }
          else {
            this._dataService.scrollToBottom = true;
            this._dataService.messages = [message, ...this.messages];
            this._dataService.messagesCountChannel = this.totalMessages + 1;
          }
          this.form?.get('input')?.setValue('');
        })
        .catch(error => this._dataService.error = error.message)
        .finally(() => this.sendButtonDisabled = false);
    } else {
      if (!confirm(this._translateService.instant('IM.AreYouSure'))) {
        this.sendButtonDisabled = false;
        return;
      }

      const message: IMessagePost = {
        body: input,
        isReply: false,
        hasReply: false,
      }

      this._context
        .model(`${model}/${this.selectedId}/notify`)
        .save(message)
        .then(results => {
          const message = {
            ...results,
            reactions: [],
            sender: { ...this.user },
          };
          this._dataService.scrollToBottom = true;
          this._dataService.messages = [message, ...this.messages];
          this._dataService.messagesCountChannel = this.totalMessages + 1;
          this.form?.get('input')?.setValue('');
          this._toastrService.success(this._translateService.instant('IM.InstantNotificationSent'));
        })
        .catch(error => this._dataService.error = error.message)
        .finally(() => this.sendButtonDisabled = false);
    }
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}
