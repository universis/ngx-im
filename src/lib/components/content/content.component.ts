import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { Subject } from 'rxjs';
import { first, takeUntil } from 'rxjs/operators';
import { IMember } from '../../interfaces/IMember';
import { IMessage } from '../../interfaces/IMessage';
import { IReaction, IReactionPost } from '../../interfaces/IReaction';
import { DataService } from '../../services/data.service';
import { IAccount } from '../../interfaces/IAccount';
import { AppEventService } from '@universis/common';

const PAGE = 20;
@Component({
  selector: 'im-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ContentComponent implements OnInit, OnDestroy {
  loadingChannel: boolean = false;
  loadingMessages: boolean = false;
  loadingInitMessages: boolean = false;
  index: number = 0;
  disabled: boolean = false;
  selectedId!: number;
  messages: IMessage[] = [];
  user!: IMember;
  total!: number;

  private _unsubscribeAll: Subject<void>;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _dataService: DataService,
    private _context: AngularDataContext,
    private _appEvent: AppEventService
  ) {
    this._unsubscribeAll = new Subject();
  }

  ngOnInit(): void {
    this._appEvent.added.subscribe((event: any) => {
      // Message Add
      if (event.entityType === 'InstantMessage') {
        const entity = event.entity;
        if (entity.recipient === this.selectedId) {
          this.loadNewMessage(entity.identifier);
        }
      }
    });

    this._appEvent.changed.subscribe((event: any) => {
      // Reaction Add/Remove
      if (event.entityType === 'InstantMessageReaction') {
        const entity = event.entity;
        if (entity.instantMessage.recipient === this.selectedId) {
          this.reloadMessage(entity.instantMessage.identifier)
        }
      }

      // Message Update
      if (event.entityType === 'InstantMessage') {
        const entity = event.entity;
        if (entity.recipient === this.selectedId) {
          this.reloadMessage(entity.identifier)
        }
      }
    });

    this._appEvent.removed.subscribe((event: any) => {
      // Message Delete
      if (event.entityType === 'InstantMessage') {
        const entity = event.entity;
        const newMessages = this.messages.filter(x => x.identifier !== entity.identifier);
        this._dataService.messages = [...newMessages];
        this._dataService.messagesCountChannel = this.total ? this.total - 1 : 0;
      }
    });

    this._dataService._account$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(res => {
        const data: IAccount | any = { ...res };
        const { imAccount } = data;
        this.user = imAccount;
      });

    this._activatedRoute.params
      .subscribe((paramsId: any) => {
        const selectedId = paramsId.id;
        if (selectedId) {
          this.index = 0;
          this.selectedId = parseInt(selectedId);
          this.fetchChannelMembers();
          this.fetchChannelMessages();
        };
      });

    this._dataService._messages$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(val => {
        this.messages = val || [];
      });

    this._dataService._messagesCountChannel$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(res => {
        this.total = res;
      });
  }

  loadNewMessage(identifier: string) {
    this._context
      .model(`MessagingChannels/${this.selectedId}/messages`)
      .where('identifier').equal(identifier)
      .expand('createdBy', 'sender', 'reactions', 'reactions($expand=user)')
      .take(1)
      .getItem()
      .then(res => {
        const message = res;
        if (message) {
          if (!message.isReply) {
            this._dataService.messages = [message, ...this.messages];
            this._dataService.messagesCountChannel = this.total ? this.total + 1 : 1;
            if (!this._dataService.disableScrollToBottom) this._dataService.scrollToBottom = true;
          }
        }
      })
      .catch(error => this._dataService.error = error.message)
  }

  reloadMessage(identifier: string) {
    this._context
      .model(`MessagingChannels/${this.selectedId}/messages`)
      .where('identifier').equal(identifier)
      .expand('createdBy', 'sender', 'reactions', 'reactions($expand=user)')
      .take(1)
      .getItem()
      .then(res => {
        const message = res;
        if (message) {
          if (!message.isReply) {
            const newMessages = this.messages.map(x => x.id === message.id ? message : x);
            this._dataService.messages = [...newMessages];
          }
        }
      })
      .catch(error => this._dataService.error = error.message)
  }

  fetchChannelMembers() {
    this._dataService._channels$
      .pipe(takeUntil(this._unsubscribeAll), first(x => x && x.length > 0))
      .subscribe(val => {
        this.loadingChannel = true;
        const channel = (val || []).find(x => x.id === this.selectedId);
        if (!channel) this.loadingChannel = false;
        else {
          this._context
            .model(`MessagingChannels/${channel.id}/members`)
            .getItems()
            .then(result => {
              this._dataService.selectedChannel = {
                ...channel,
                members: result || []
              };
            })
            .catch(error => this._dataService.error = error.message)
            .finally(() => this.loadingChannel = false);
        }
      });
  }

  fetchChannelMessages() {
    if (this.index === 0) this.loadingInitMessages = true;
    this.loadingMessages = true;
    this._context
      .model(`MessagingChannels/${this.selectedId}/messages`)
      .where('isReply')
      .equal(0)
      .expand('createdBy', 'sender', 'reactions', 'reactions($expand=user)')
      .skip(PAGE * this.index)
      .take(PAGE)
      .orderByDescending('dateCreated')
      .getList()
      .then(results => {
        const { value, total } = results;
        if (this.index === 0) {
          this._dataService.messagesCountChannel = total;
          this._dataService.messages = [];
        }
        this._dataService.messages = [...this.messages, ...value]
        this.disabled = value.length < PAGE;
        this.index++;
      })
      .catch(error => {
        this.disabled = true;
        this._dataService.error = error.message
      })
      .finally(() => {
        this.loadingInitMessages = false;
        this.loadingMessages = false
      });
  }

  fetchMore() {
    if (!this.disabled) this.fetchChannelMessages();
  }

  reactionAction(event: { message: IMessage, index: number }) {
    const { message, index } = event;
    const reaction: IReactionPost = {
      reaction: index,
      instantMessage: message.id
    }
    const tempDisabled = this.disabled;
    this.disabled = true;
    this._context
      .model(`MessagingChannels/${this.selectedId}/toggleReaction`)
      .save(reaction)
      .then((results: IReaction) => {
        const newMessage: IMessage | any =
          results ? {
            ...message,
            reactions: [
              ...message.reactions,
              {
                ...results,
                user: this.user
              }
            ].sort((x, y) => x.id - y.id)
          } : {
            ...message,
            reactions: message.reactions.filter(x => !(x.reaction === index && x.user.id === this.user?.id))
          }
        const newMessages = this.messages.map(x => x.id === newMessage.id ? newMessage : x);
        this._dataService.messages = [...newMessages];
      })
      .catch(error => this._dataService.error = error.message)
      .finally(() => this.disabled = tempDisabled);
  }

  deleteMessage(event: { message: IMessage }) {
    const { message } = event;
    const tempDisabled = this.disabled;
    this.disabled = true;
    this._context
      .model(`MessagingChannels/${this.selectedId}/removeMessage`)
      .save(message)
      .then((results: { item: IMessage; children: IMessage }) => {
        const { item } = results;
        this._dataService.messages = [...this.messages].filter(x => x.id !== item.id);
        this._dataService.messagesCountChannel = this.total ? this.total - 1 : 0;
      })
      .finally(() => this.disabled = tempDisabled);
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}
