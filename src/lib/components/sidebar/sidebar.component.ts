import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { IChannel } from '../../interfaces/IChannel';
import { ISidebarItemNav, ISidebarNav } from '../../interfaces/ISidebarNav';
import { DataService } from '../../services/data.service';
import * as _ from 'lodash';
import { IAccount, IGroup } from '../../interfaces/IAccount';
import { TranslateService } from '@ngx-translate/core';
import { ImPreferencesService } from '../../services/im-preferences.service';
import { ChannelType, MessagingChannelTypes } from '../../interfaces/MessagingChannelTypes';
import { AddChannelService } from '../../services/add-channel.service';
import { Router } from '@angular/router';

@Component({
  selector: 'im-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent implements OnInit, OnDestroy {
  @Output() openAddChannelModal = new EventEmitter<ChannelType>();

  @Input() loading!: boolean;

  channels: IChannel[] = [];
  menusInit: ISidebarNav[] = [];
  menus: ISidebarNav[] = [];
  searchValue: string = '';
  init: boolean = true;
  isAdmin: boolean = false;
  isInstructor: boolean = false;
  openSidebar: boolean = false;

  private _unsubscribeAll: Subject<void>;

  constructor(
    private _dataService: DataService,
    private _translateService: TranslateService,
    private _imPreferencesService: ImPreferencesService,
    private _addChannelService: AddChannelService,
    private _router: Router
  ) {
    this._unsubscribeAll = new Subject();
  }

  private _mapper = (x: IChannel) => ({
    headline: x.headline,
    keywords: x.keywords,
    alternateName: x.alternateName,
    maintainer: x.maintainer,
    readOnly: x.readOnly,
    id: x.id,
  });

  ngOnInit() {
    this._dataService._openSidebar$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(val => this.openSidebar = val);

    this._dataService._account$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(res => {
        const data: IAccount | any = { ...res };
        this.isAdmin = !_.isEmpty(_.find(data.groups, (x: IGroup) => x.name === 'Administrators'));
        this.isInstructor = !_.isEmpty(_.find(data.groups, (x: IGroup) => x.name === 'Instructors'));
      });

    this._dataService._channels$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(val => {
        this.channels = val || [];

        const plainChannels = this.channels.filter(x => x.additionalType === MessagingChannelTypes.CHANNEL);
        const direct = this.channels.filter(x => x.additionalType === MessagingChannelTypes.DIRECT);
        const courseClass = this.channels.filter(x => x.additionalType === MessagingChannelTypes.COURSE_CLASS);
        const studentThesis = this.channels.filter(x => x.additionalType === MessagingChannelTypes.STUDENT_THESIS);

        this.menusInit = [
          {
            title: this._translateService.instant('IM.Channels'),
            type: MessagingChannelTypes.CHANNEL,
            collapse: this._imPreferencesService.getPreference(MessagingChannelTypes.CHANNEL) ?? false,
            children: plainChannels.map(channel => this._mapper(channel))
          },
          {
            title: this._translateService.instant('IM.CourseClasses'),
            type: MessagingChannelTypes.COURSE_CLASS,
            collapse: this._imPreferencesService.getPreference(MessagingChannelTypes.COURSE_CLASS) ?? false,
            children: courseClass.map(channel => this._mapper(channel))
          },
          {
            title: this._translateService.instant('IM.StudentTheses'),
            type: MessagingChannelTypes.STUDENT_THESIS,
            collapse: this._imPreferencesService.getPreference(MessagingChannelTypes.STUDENT_THESIS) ?? false,
            children: studentThesis.map(channel => this._mapper(channel))
          },
          {
            title: this._translateService.instant('IM.DirectMessages'),
            type: MessagingChannelTypes.DIRECT,
            collapse: this._imPreferencesService.getPreference(MessagingChannelTypes.DIRECT) ?? false,
            children: direct.map(channel => this._mapper(channel))
          }
        ];
        this.onSearchChange(this.searchValue);
      });
  }

  checkTitleAndChannelKeywords(sideNavItem: ISidebarItemNav) {
    const keywords = sideNavItem.keywords || [];
    const title = sideNavItem.headline || "";
    const alternateName = sideNavItem.alternateName || "";
    const isIncluded = keywords.find(x => x.toLowerCase().indexOf(this.searchValue) > -1) || title.toLowerCase().indexOf(this.searchValue) > -1 || alternateName.toLowerCase().indexOf(this.searchValue) > -1
    return isIncluded;
  }

  onSearchChange(event: any) {
    this.searchValue = (typeof event === 'string') ? event : event.target.value.toLowerCase().trim();
    this.menus = this
      .menusInit
      .map(x => ({
        ...x,
        children: x.children.filter(y => this.checkTitleAndChannelKeywords(y))
      }))
    this.init = false;
  }

  collapseMenus(index: number) {
    this.menus[index].collapse = !this.menus[index].collapse;
    this._imPreferencesService.setPreference(this.menus[index].type, this.menus[index].collapse);
  }

  openAddChannel(index: number) {
    const menu = this.menus[index];
    const { type } = menu;
    this.openAddChannelModal.emit(type);
  }

  openAddChannelRouter(index: number) {
    const menu = this.menus[index];
    const { type } = menu;
    this._addChannelService.openModal(type, this.updateChannelsState);
  }

  // use arrow function to have access to "this" keyword
  updateChannelsState = (newChannel: IChannel) => {
    try {
      const contains = this.channels.some(x => x.id === newChannel.id);
      if (contains) {
        this._dataService.channels = this.channels.map(x => x.id === newChannel.id
          ? Object.assign({}, { ...x, ...newChannel }, null)
          : x
        );
      } else {
        this._dataService.channels = [{ ...newChannel }, ...this.channels];
      }
      this._dataService.selectedChannel = newChannel;
      this._router.navigate(['im', 'room', newChannel.id]);
    } catch (err) {
      console.error(err);
    }
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}
