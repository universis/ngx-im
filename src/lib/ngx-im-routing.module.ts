import { NgModule } from '@angular/core';
import { LayoutComponent } from './layout/layout.component';
import { RouterModule, Routes } from '@angular/router';
import { ContentComponent } from './components/content/content.component';
import { EmptyContentComponent } from './components/empty-content/empty-content.component';
import { NgxImComponent } from './ngx-im.component';
import { AddChannelRouterComponent } from './components/modals/add-channel-router/add-channel-router.component';
import { AdvancedFormModalComponent, AdvancedFormModalData, AdvancedFormModalOptions, AdvancedFormParentItemResolver } from '@universis/forms';

const routes: Routes = [
  {
    path: '',
    component: NgxImComponent,
    children: [
      {
        path: '',
        component: LayoutComponent,
        children: [
          {
            path: '',
            component: EmptyContentComponent
          },
          {
            path: 'room',
            component: EmptyContentComponent
          },
          {
            path: 'room/:id',
            component: ContentComponent
          },
          {
            path: 'new',
            children: [
              {
                path: 'MessagingChannel',
                pathMatch: 'full',
                component: AdvancedFormModalComponent,
                outlet: 'modal',
                data: <AdvancedFormModalData> {
                  model: 'MessagingChannels',
                  action: 'new',
                  closeOnSubmit: true,
                  continueLink: '/im',
                }
              },
              {
                path: 'DirectMessagingChannel',
                pathMatch: 'full',
                component: AdvancedFormModalComponent,
                outlet: 'modal',
                data: <AdvancedFormModalData> {
                  model: 'DirectMessagingChannels',
                  action: 'new',
                  closeOnSubmit: true,
                  continueLink: '/im',
                }
              },
              {
                path: 'CourseClassMessagingChannel',
                pathMatch: 'full',
                component: AdvancedFormModalComponent,
                outlet: 'modal',
                data: <AdvancedFormModalData> {
                  model: 'CourseClassMessagingChannels',
                  action: 'new',
                  closeOnSubmit: true,
                  continueLink: '/im',
                }
              },
              {
                path: 'StudentThesisMessagingChannel',
                pathMatch: 'full',
                component: AdvancedFormModalComponent,
                outlet: 'modal',
                data: <AdvancedFormModalData> {
                  model: 'StudentThesisMessagingChannels',
                  action: 'new',
                  closeOnSubmit: true,
                  continueLink: '/im',
                }
              }
            ]
          },
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class NgxImRoutingModule { }
