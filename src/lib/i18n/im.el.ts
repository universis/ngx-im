export const el = {
  IM: {
    Forms: {
      MessagingChannel: 'Απλό κανάλι μηνυμάτων',
      CourseClassMessagingChannel: 'Κανάλι τάξης μαθήματος',
      StudentThesisMessagingChannel: 'Κανάλι διπλωματικής εργασίας',
      DirectMessagingChannel: 'Κανάλι άμεσων μηνυμάτων',
    },
    Errors: {
      SomethingWentWrong: 'Κάτι πήγε στραβά',
      NoMemberInfo: 'Δεν βρέθηκαν πληροφορίες για το μέλος',
      MemberNotFound: 'Δεν βρέθηκε το μέλος',
      MemberNotAStudent: 'Το μέλος δεν είναι φοιτητής',
      MemberNotAnInstructor: 'Το μέλος δεν είναι διδάσκων',
      InvalidForm: 'Μη έγκυρη φόρμα',
    },
    Channels: 'Κανάλια',
    DirectMessages: 'Άμεσα Μηνύματα',
    Search: 'Αναζήτηση...',
    SelectChannel: 'Επιλέξτε ένα κανάλι!',
    Members: 'Μέλη',
    Member: 'Μέλος',
    Media: 'Αρχεία',
    ManageChannelMembers: 'Διαχείριση μελών',
    EditChannel: 'Επεξεργασία καναλιού',
    AddChannel: 'Προσθήκη καναλιού',
    HideChannel: 'Απόκρυψη καναλιού',
    TypeMessage: 'Πληκτρολογήστε...',
    NoMessagesFound: 'Δεν υπάρχουν μηνύματα...',
    Messages: 'Μηνύματα',
    Message: 'Μήνυμα',
    Send: 'Αποστολή',
    Characters: 'χαρακτήρες',
    Replies: 'Απαντήσεις',
    Reply: 'Απάντηση',
    NoReplies: 'Δεν υπάρχουν απαντήσεις',
    You: 'Εσείς',
    Tasks: 'Ενέργειες',
    Cancel: 'Ακύρωση',
    Create: 'Δημιουργία',
    Update: 'Ενημέρωση',
    Close: 'Κλείσιμο',
    Hide: 'Απόκρυψη',
    Add: 'Προσθήκη',
    Delete: 'Διαγραφή',
    Edit: 'Επεξεργασία',
    Save: 'Αποθήκευση',
    UrlIsInvalid: 'Το URL δεν είναι έγκυρο',
    Headline: 'Τίτλος',
    Description: 'Περιγραφή',
    Required: 'Υποχρεωτικό πεδίο',
    Keywords: 'Λέξεις κλειδιά',
    Type: 'Τύπος',
    AddMember: 'Προσθήκη μέλους',
    EditUser: 'Επεξεργασία χρήστη',
    Thread: 'Απαντήσεις',
    NotUnique: 'Δεν είναι μοναδικό',
    CannotBeUndoneMessage: 'Είστε σίγουροι ότι θέλετε να συνεχίσετε; Αυτή η ενέργεια δεν μπορεί να αναιρεθεί.',
    AreYouSure: 'Είστε σίγουροι;',
    WelcomeMessage: 'Άμεση Επικοινωνία',
    EnableIMAccountMessage: 'Παρακαλώ ενεργοποιήστε το λογαριασμό σας για να στείλετε μηνύματα.',
    EnableIMAccount: 'Ενεργοποίηση λογαριασμού IM',
    ReadOnly: 'Μόνο για ανάγνωση',
    ReadOnlyMessage: 'Δεν μπορείτε να στείλετε μηνύματα σε αυτό το κανάλι.',
    ReadWrite: 'Ανοιχτό κανάλι',
    AccountCreated: 'Ο λογαριασμός σας δημιουργήθηκε με επιτυχία.',
    AccountCreationFailed: 'Η δημιουργία λογαριασμού απέτυχε. Παρακαλώ δοκιμάστε ξανά.',
    InstantNotification: 'Άμεση ειδοποίηση',
    InstantNotificationDescription: 'Αποστολή μιας άμεσης ειδοποίησης στο κανάλι',
    InstantNotificationConfirmation: 'Αυτή η ενέργεια θα στείλει μια άμεση ειδοποίηση σε όλα τα μέλη που ανήκουν σε αυτό το κανάλι και έχουν συνδέσει λογαριασμούς από εξωτερικές πλατφόρμες (πχ. Viber ή Discord).',
    InstantNotificationSent: 'Η άμεση ειδοποίηση στάλθηκε με επιτυχία.',
    PopulateMembers: 'Συγχρονισμός μελών',
    CourseClasses: 'Τάξεις',
    StudentTheses: 'Διπλωματικές',
  }
}
