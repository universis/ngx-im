import { el } from './im.el';
import { en } from './im.en';

export const IM_LOCALES = {
  el,
  en
};
