import { Component, Inject, OnInit, Optional, ViewEncapsulation } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { IConfig } from './interfaces/IConfig';
import { DataService } from './services/data.service';
import { APP_CONFIG } from './ngx-im.config';
import { UserService } from '@universis/common';

const MIN_HEIGHT_IM_DEFAULT = '89vh';
const MAX_HEIGHT_IM_DEFAULT = '89vh';
const MAX_WIDTH_MESSAGES_DEFAULT = '450px';
const MIN_HEIGHT_REPLIES_DEFAULT = '50vh';
const MAX_HEIGHT_REPLIES_DEFAULT = '50vh';

@Component({
  selector: 'app-instant-messages',
  template: `<router-outlet></router-outlet>`, // <router-outlet></router-outlet>
  styles: []
})
export class NgxImComponent implements OnInit {

  constructor(private context: AngularDataContext,
    private userService: UserService,
    public dataService: DataService,
    @Optional() @Inject(APP_CONFIG) config: IConfig) {
    const user = this.userService.getUserSync();
    this.context.setBearerAuthorization(user?.token?.access_token);
    const conf: IConfig = {
      MIN_HEIGHT_IM: config?.MIN_HEIGHT_IM ?? MIN_HEIGHT_IM_DEFAULT,
      MAX_HEIGHT_IM: config?.MAX_HEIGHT_IM ?? MAX_HEIGHT_IM_DEFAULT,
      MAX_WIDTH_MESSAGES: config?.MAX_WIDTH_MESSAGES ?? MAX_WIDTH_MESSAGES_DEFAULT,
      MIN_HEIGHT_REPLIES: config?.MIN_HEIGHT_REPLIES ?? MIN_HEIGHT_REPLIES_DEFAULT,
      MAX_HEIGHT_REPLIES: config?.MAX_HEIGHT_REPLIES ?? MAX_HEIGHT_REPLIES_DEFAULT,
    }
    this.dataService.config = conf;
  }

  ngOnInit(): void { }

}
