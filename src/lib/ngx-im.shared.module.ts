import { ModuleWithProviders, NgModule } from "@angular/core";
import { InstantMessageSubscriber } from "./services/InstantMessageSubscriber";
import { CommonModule } from "@angular/common";
import { ServerEventModule, ServerEventService, SharedModule } from "@universis/common";

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ServerEventModule,
  ],
  declarations: [],
  exports: [],
  providers: [InstantMessageSubscriber],
})
export class NgxImSharedModule {
  constructor(
    private _serverEventService: ServerEventService
  ) { }

  static forRoot(): ModuleWithProviders<NgxImSharedModule> {
    return {
      ngModule: NgxImSharedModule,
      providers: [
        InstantMessageSubscriber,
      ]
    };
  }
}