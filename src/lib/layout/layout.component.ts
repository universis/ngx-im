import { Component, OnDestroy, OnInit, TemplateRef, ViewChild, ViewEncapsulation } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AddChannelComponent } from '../components/modals/add-channel/add-channel.component';
import { IChannel } from '../interfaces/IChannel';
import { DataService } from '../services/data.service';
import { IAccount } from '../interfaces/IAccount'
import { IMember } from '../interfaces/IMember';
import { ViewMemberComponent } from '../components/modals/view-member/view-member.component';
import { AppEventService, LoadingService, ModalService } from '@universis/common';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { IAttachment } from '../interfaces/IAttachment';
import { ViewAttachmentComponent } from '../components/modals/view-attachment/view-attachment.component';
import { ChannelType } from '../interfaces/MessagingChannelTypes';
import { Router } from '@angular/router';

@Component({
  selector: 'im-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LayoutComponent implements OnInit, OnDestroy {
  @ViewChild(AddChannelComponent) addChannelModal!: AddChannelComponent;
  @ViewChild(ViewMemberComponent) viewMemberModal!: ViewMemberComponent;
  @ViewChild(ViewAttachmentComponent) viewAttachmentModal!: ViewAttachmentComponent;

  channels: IChannel[] = [];
  openSidebar: boolean = false;
  openSidebarRight: boolean = false;
  loading: boolean = false;

  private _unsubscribeAll: Subject<void>;

  constructor(
    public dataService: DataService,
    private _context: AngularDataContext,
    private _loadingService: LoadingService,
    private _modalService: ModalService,
    private _toastrService: ToastrService,
    private _translateService: TranslateService,
    private _router: Router,
    private _appEvent: AppEventService
  ) {
    this._unsubscribeAll = new Subject();
    this._modalService.config.keyboard = true;
    this._modalService.config.ignoreBackdropClick = false;
  }

  ngOnInit() {
    this._appEvent.added.subscribe((event: any) => {
      // Channel Add
      if (event.entityType === 'MessagingChannel') {
        const entity = event.entity;
        this.loadChannel(entity.id);
      }
    })

    this._appEvent.changed.subscribe((event: any) => {
      // Channel Update
      // This works for channel "removal" as well, since the channel is marked as hidden
      if (event.entityType === 'MessagingChannel') {
        const entity = event.entity;
        this.loadChannel(entity.id);
      }
    });

    this._loadingService.showLoading();
    this._context.model('users/me')
      .asQueryable()
      .expand('imAccount', 'groups')
      .getItem()
      .then(res => {
        const user: IAccount = { ...res };
        this.dataService.account = { ...user };
      })
      .finally(() => this._loadingService.hideLoading());

    this.dataService._channels$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(val => this.channels = val);

    this.dataService._openSidebar$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(val => this.openSidebar = val);

    this.dataService._openSidebarRight$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(val => this.openSidebarRight = val);

    this.fetchChannels();
  };

  openModal(template: TemplateRef<any>) {
    this._modalService.openModal(template);
  }

  loadChannel(id: number) {
    this._context.model('MessagingChannels')
      .select('id', 'alternateName', 'additionalType', 'maintainer', 'dateCreated', 'headline', 'isDisabled', 'readOnly', 'type')
      .where('id').equal(id)
      .expand('keywords')
      .getItem()
      .then(res => {
        const newChannel: IChannel = { ...res };

        this._appEvent.change.next({
          action: 'update-current-channel',
          data: {
            channel: newChannel
          }
        });

        if (newChannel.isDisabled) {
          const newChannels = this.channels.filter(x => x.id !== newChannel.id);
          this.dataService.channels = [...newChannels];
          if (this.dataService.selectedChannel?.id === newChannel.id) {
            this._router.navigate(['im']);
          }
        } else {
          const contains = this.channels.some(x => x.id === newChannel.id);
          if (contains) {
            this.dataService.channels = this.channels.map(x => x.id === newChannel.id
              ? Object.assign({}, { ...x, ...newChannel }, null)
              : x
            );
          } else {
            this.dataService.channels = [{ ...newChannel }, ...this.channels];
          }
        }
      })
      .catch(error => this.dataService.error = error.message);
  }

  fetchChannels() {
    this.loading = true;
    this._context
      .model('MessagingChannels')
      .select('id', 'alternateName', 'additionalType', 'maintainer', 'dateCreated', 'headline', 'isDisabled', 'readOnly', 'type')
      .where('isDisabled').notEqual(true)
      .orderByDescending('dateCreated')
      .expand('keywords')
      .getItems()
      .then(results => this.dataService.channels = results)
      .catch(error => this.dataService.error = error.message)
      .finally(() => this.loading = false);
  }

  createIMAccount() {
    this._loadingService.showLoading();

    const imAccountName = this.dataService.account?.name?.split('@')[0];
    if (!imAccountName) {
      this._loadingService.hideLoading();
      return;
    }

    this._context.model('InstantMessageAccounts')
      .save({
        name: imAccountName,
        account: this.dataService.account?.id
      })
      .then(res => {
        this._toastrService.success(this._translateService.instant('IM.AccountCreated'));
      })
      .catch(err => {
        this._toastrService.error(this._translateService.instant('IM.AccountCreationFailed'));
      })
      .finally(() => this._loadingService.hideLoading());
  }

  openAddChannel(type: ChannelType) {
    this.addChannelModal.openAddChannelModal(type);
  }

  openViewMemberModal(member: IMember) {
    this.viewMemberModal.openModal(member);
  }

  openViewAttachmentModal(attachment: IAttachment) {
    this.viewAttachmentModal.openModal(attachment);
  }

  newChannel(channel: IChannel) {
    this.dataService.channels = [
      { ...channel },
      ...this.channels
    ];
    this._router.navigate(['im', 'room', channel.id]);
  }

  styleObject = (): Object => ({ minHeight: `${this.dataService.config?.MIN_HEIGHT_IM}`, maxHeight: `${this.dataService.config?.MAX_HEIGHT_IM}` })

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}
