import { IMember } from "./IMember";
import { INewUser } from "./INewUser";

export interface IChannel {
  alternateName: string;
  dateCreated: Date;
  dateModified: Date;
  readOnly: boolean;
  expiresAt: string;
  headline: string;
  maintainer?: IMember;
  keywords?: string[];
  id: number;
  members: IMember[];
  type: 1 | 2;
  additionalType: string;
  isDisabled: boolean;
}

export interface IChannelPost {
  alternateName: string;
  headline: string;
  keywords: string[];
  members: INewUser[];
  readOnly: boolean;
}

export interface IChannelHide {
  id: number;
  isDisabled: boolean;
}

/**
 * Other channel types
 */
// Course Class
export interface ICourseClassChannel extends IChannel {
  courseClass: number;
}

export interface ICourseClassChannelPost extends IChannelPost {
  courseClass: number;
}

// Student Theses
export interface IStudentThesisChannel extends IChannel {
  studentThesis: number;
}

export interface IStudentThesisChannelPost extends IChannelPost {
  studentThesis: number;
}