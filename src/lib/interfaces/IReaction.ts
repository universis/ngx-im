import { IMember } from "./IMember";

export interface IReaction {
  id: number;
  reaction: number;
  user: IMember;
  instantMessage: number;
}

export interface IReactionPost {
  reaction: number;
  instantMessage: number;
}

export const ReactionType = {
  LIKE: 1,
  SUPER: 2,
};