import { IAttachment } from "./IAttachment";
import { IMember } from "./IMember";
import { MessagingChannelTypes } from "./MessagingChannelTypes";

export type ISidebarItemNav = {
  id: number
  headline: string
  keywords?: string[]
  alternateName: string
  maintainer?: IMember
  readOnly: boolean
}

export type ISidebarRightItemNav = IMember | IAttachment;

export type ISidebarNav = {
  title: string
  // type: 'channel' | 'direct'
  type: (typeof MessagingChannelTypes)[keyof typeof MessagingChannelTypes]
  collapse: boolean
  children: ISidebarItemNav[]
}

export type ISidebarRightNav = {
  title: string
  type: 'members' | 'media'
  collapse: boolean
  children: ISidebarRightItemNav[]
}