export interface IAttachmentType {
  id: number;
  identifier: string;
  alternateName: string;
  physical: boolean;
}