import { IAttachmentType } from "./IAttachmentType";

export interface IAttachment {
  id: number;
  additionalType: string;
  alternateName: string;
  contentType: string;
  datePublished: Date;
  published: boolean;
  keywords: string;
  thumbnail: string;
  version: string;
  attachmentType: IAttachmentType;
  owner: number;
  // actions: IAcceptAttachmentAction[];
  createdBy: number;
  dateCreated: Date;
  dateModified: Date;
}