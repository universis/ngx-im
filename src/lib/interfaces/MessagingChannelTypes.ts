export enum MessagingChannelTypes {
  CHANNEL = "MessagingChannel",
  DIRECT = "DirectMessagingChannel",
  COURSE_CLASS = "CourseClassMessagingChannel",
  STUDENT_THESIS = "StudentThesisMessagingChannel",
};

export type ChannelType = (typeof MessagingChannelTypes)[keyof typeof MessagingChannelTypes];
